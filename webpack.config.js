const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
//const TerserWebpackPlugin = require('terser-webpack-plugin')
//const HTMLWebpackPlugin = require('html-webpack-plugin')

const isDev = process.env.NODE_ENV === "development"

module.exports = {
    context: path.resolve(__dirname, 'src'), //куда заглянет webpack
    mode: 'development',
    entry: { //точка входа, чанки
        main: ['@babel/polyfill', './index.js'],
        //func: '../js/func.js'
    },
    output: {
        filename: '[name].bundle.js', //'[name].[contenthash].js' против кешарования
        path: path.resolve(__dirname, 'dist') //"C:\\evolution\\kuhniexpress\\KUHNIEXPRESS\\html\\js" //
    },
    resolve: {
        extensions: ['.js'], //расширения
        alias: { // укороченные пути 
            '@js': path.resolve(__dirname, 'src/js'),
            '@': path.resolve(__dirname, 'src')
        }
    },
    //Оптимизация, убираем дважды исп. jquery в только 1 vendor
    /*optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },*/
    //исходные карты или исходный код
    /*devtool: isDev ? 'source-map' : '',*/
    plugins: [
        //вынос Css в отдельный файл
        /*new MiniCssExtractPlugin({
            filename: '[name].bundle.css'
        }),*/
        //Скорировать статичные файлы в dist
        /*new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'src/css/'), //прям папку
                to: path.resolve(__dirname, 'dist/css/')
            }
        ]),*/
        /*new HTMLWebpackPlugin({ //для взаимодействия с html
            template: './src/000074_KUHNIEXPRESS.htm',
        })*/
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules//*, path.resolve(__dirname, 'src/js')*/], //игнор папки
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                '@babel/preset-env',
                                '@babel/preset-typescript'
                            ],
                            plugins: ['@babel/plugin-proposal-class-properties',]
                        },
                    }
                ]
            }
            /*{ //препроцессоры
                test: /\.less$/,
                use: [{
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        hrm: isDev,
                        reloadAll: true
                    },
                },
                    'css-loader',
                    'less-loader']
            },*/
            /*{ //вынос Css в отдельный файл
                test: /\.css$/,
                use: [{
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        hrm: isDev,
                        reloadAll: true
                    },
                }, 'css-loader']
            },*/
            /*test: /\.css$/, //обработчик css типа import file.css
            use: ['style-loader', 'css-loader'] //справа налево
        */
            /*{
                //обработчик картинок типа import file.jpg
                test: /\.(png|jpg|svg|jpeg|gif)$/,
                use: ['file-loader']
            }*/
        ]
    },
    //Cntrl + c
    devServer: {
        port: 4200
    }
}
