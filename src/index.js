﻿/*
    Точка входа
*/
import * as $ from 'jquery'
var _ = require('lodash'); //import * as _ from 'lodash'
import mui from '@js/mui.js'
import X2JS from '@js/xml2json.js'
import Riga from '@js/classRiga.js'
import { stringToMap, upInt, compareNumeric, compareNumbers, SaveFile, b64EncodeUnicode } from '@js/func.js'
import { rigaFunc } from '@js/rigaFunc.js'
import { tableTopDivider } from '@js/tableTopDivider.js'
import { print, printTable } from '@js/print.js'
import { CheckPrice } from '@js/CheckPrice.js'


var x2js = new X2JS();

//буква ю
XML_FILE = XML_FILE.split("0XFE").join("Ю");
XML_FILE = XML_FILE.split("0xfe").join("ю");

export var jsonPrev = (x2js.xml_str2json(XML_FILE))
export let RIGHE = jsonPrev.PREV.RIGHE

delete jsonPrev.PREV.TESTA.VAR.__text;
var classRiga = new Riga(RIGHE);
jsonPrev.PREV.RIGHE.__proto__ = classRiga;
console.log(jsonPrev);

//if (dis_fio === "Галимов Алмаз Равилевич") _saveojecdprt = "1";
if (EMAIL === "_KEY_3920" || EMAIL === "a.galimov@kuhni-express.ru"
    || EMAIL === "npl83@mail.ru") {
    _savedproject = "1";
}

//window.r = RIGHE; //raw
RIGHE.init();

for (const key in RIGHE) {
    if (RIGHE.hasOwnProperty(key)) {
        const el = RIGHE[key];
        if (el.COD == "PLINTUS") el.DES = ""
    }
}

//обработка riga
rigaFunc(RIGHE, jsonPrev);
window.f = RIGHE;
window.d = jsonPrev;

//модуль длинномеров
tableTopDivider(RIGHE);


//печать
print(jsonPrev);

//сохраняем в XML
$("#SaveFile").click((e) => {
    SaveFile(jsonPrev, e)
});

//$("#SaveFile").addEventListener('click', SaveFile)
$("#SaveFile").attr("disabled", _savedproject !== "1");

//спецификация
export function specification(_jsonPrev) {
    let RIGHE = _jsonPrev.PREV.RIGHE;
    let tableTr = `
    <thead>
        <tr>
            <th class="styleTableHead" width="15px">№</th>
            <th class="styleTableHead" width="70px">Код</th>
            <th class="styleTableHead" width="520px">Наименование</th>
            <th class="styleTableHead" width="10px">ШхВхГ</th>
            <th class="styleTableHead" width="15px"><span>Кол-во</span></th>
            <th class="styleTableHead" width="20px"><span>Артикул</span></th>
        <tr>
    </thead>`

    //сортировка по IDBOX
    let obj = {}
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            const el = RIGHE[key];
            const n = el.IDBOX;
            if (!obj[n]) obj[n] = {} //создание группы


            if (isFirst(el) && !obj[n]["0"]) {
                obj[n]["0"] = el;
            } else {
                obj[n][key] = el;
            }
        }
    }
    //console.log(obj);
    window.t = obj;

    let n = 1
    /*for (const key in jsonPrev.PREV.RIGHE) {
        if (jsonPrev.PREV.RIGHE.hasOwnProperty(key)) {
            */
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            const el = obj[key];
            //console.log(el);
            for (const jey in el) {
                if (el.hasOwnProperty(jey)) {
                    const a = el[jey];
                    const myArticle = RIGHE.getTag(a, 'MYARTICLE');//findTag(a.VAR, 'MYARTICLE');
                    let bold = "";
                    if (isFirst(a)) bold = "mainT";

                    tableTr += `
                    <tr id="riga${n}" class="${bold}" data-riga="${jey}" data-uniId="${a.MY_UNI_ID}">
                        <td class="number" width="15px">${n}</td>
                        <td class="code" width="70px">${a.COD}</td>
                        <td class="des" width="520px">${a.MY_DES}</td>
                        <td class="dim" width="10px">${a.L}x${a.A}x${a.P}</td>
                        <td class="count" width="15px">${a.PZ}</td>
                        <td class="article" width="20px">${myArticle}</td>
                    </tr>`
                    n++;
                    //console.log(key, a);
                }
            }
        }
    }

    document.getElementById("myTbl").innerHTML = `
    <table id="mytable" class="mui-table mui-table--bordered">
        ${tableTr}
    </table>`

    function isFirst(el) {
        return el.MODELLO !== "" || el.COD == "TOP" || el.COD == "PLINTUS"
            || el.COD == "SOCLE" || el.COD == "KARNIZ" || el.COD == "KARNIZN"
            || el.COD == "BALUSTRADA" || el.COD == "STP"
            || RIGHE.getTag(el, "KORPUSNAME") !== ""
    }

    printTable(_jsonPrev.PREV);
}
specification(jsonPrev);


CheckPrice();



