﻿/*
    Печать
*/
import * as $ from 'jquery'

let jsonPrev = {};
let RIGHE = {};
export function print(_jsonPrev) {
    jsonPrev = _jsonPrev;
    RIGHE = jsonPrev.PREV.RIGHE;
    drawImgPreview($(".addprint"), IMG_LIST.split(';'));
    printInfo($('.InfoPrint'), _jsonPrev.PREV)
    //printTable(_jsonPrev.PREV)

    let all = document.querySelectorAll('.imgBox')
    //console.log(all);
    printImg();

    for (let i = 0; i < all.length; i++) {
        const el = all[i];
        el.addEventListener('click', printImg)
    }
}

//TODO: 12/12/2019 yurii
//Добавление предпросмотра для формы.
//
function drawImgPreview(elm, ImgList) {
    var htmltext = "";
    //Перебираем массив изображений
    for (var i = 0; i < ImgList.length; i++) {
        if (ImgList[i] && ImgList[i] !== "") {
            htmltext += '<div class="imgBox">\
			<img src="..\\..\\'+ ImgList[i] + '">\
			<input type="checkbox" checked class="checkImgI"  value="..\\..\\'+ ImgList[i] + "?v=" + Math.random().toString(36).substr(2, 9) + '">\
		</div>';
        } //onclick="printImg()"
    }
    //добавляем изображения на форму.
    if (htmltext !== "") {
        elm.html(htmltext);
    }
}

//TODO: 16/12/2019 yurii
//Добавление картинок для печати
//
export function printImg() {
    var ImgChekElement = $('.checkImgI')
    var Cod1S = c_cod;
    var lblData = jsonPrev.PREV.TESTA.DATA;
    var htmltext = "";
    for (var n = 0; n < ImgChekElement.length; n++) {
        if (ImgChekElement[n].checked) {
            if (n == 0) {
                htmltext += '\
				<div class="wrapPageI">\
				    <div class = "imgArg">\
				    <div class = "txtCentr">Приложение №3 к договору № '+ Cod1S + ' от ' + lblData + '</div>\
					<img src="'+ ImgChekElement[n].value + '">\
					</div>\
				</div>';
            } else {
                htmltext += '\
				<div class="wrapPageI">\
				    <div class = "imgArg">\
					<img src="'+ ImgChekElement[n].value + '">\
					</div>\
				</div>';
            }
            /*htmltext += '\
            <div class="wrapPageI">\
                <div class = "imgArg">\
                <img src="'+ ImgChekElement[n].value + '">\
                </div>\
            </div>';*/
            //height="99%" style="max-height:170mm" 
            //height="99%" style="max-height:175mm" 
            //htmltext += `<img src="${ImgChekElement[n].value}">`
        }
    }
    //console.log(ImgChekElement);

    $('.imgPrint').html(htmltext);
}

//TODO: 16/12/2019 yurii
//Добавление информации по заказу
function printInfo(elm, PREV) {
    var lblNumber = PREV.TESTA.VAR["C_COD"];//PREV.TESTA.NUMERO;
    var lblData = PREV.TESTA.DATA;
    var lblDis = PREV.TESTA.VAR.FIODIS.split(',')[2];
    var lblPokup = PREV.TESTA.VAR.B_FIO;
    lblDis = dis_fio;
    lblNumber = c_cod;
    //console.log(c_cod);

    var htmltext = '<div><table class="titleBlock">\
				<tr><td width = "150px">Дата: '+ lblData + '</td> <td colspan="2">№ Заказа: ' + lblNumber + '</td><td width = "150px">Версия: ' + dateversion + '</td></tr>\
				<tr><td>Дизайнер: </td><td colspan="3">'+ lblDis + '</td> </tr>\
				<tr><td colspan="4">Схема проекта (эскиз) мне понятна,мною проверена. С размерами,расположением,декоративными элементами и цветом комплектующих,в том числе:\
                  столешницы,фальш-панели,фасадов (и их фрезеровкой) ознакомлен и согласен</td></tr>\
				<tr><td>Покупатель: </td><td width = "300px">'+ lblPokup + '</td><td width = "100px">Подпись </td><td> </td></tr>\
			</table></div>';
    elm.html(htmltext);

}

//TODO: 16/12/2019 yurii
//
export function printTable(PREV) {
    var elm = $('.tablePrint');
    var htmltext = '<div class="lblSpec">Перечень позиций</div>'
    htmltext += '<div><table class="tblInfo">\
	  <thead>\
	  <th>Описание</th>\
	  <th width="10%">Размеры</th>\
	  <th width="5%">Кол-во</th>\
	  </thead>';
    var OrdinaOld, Note; //СОРТИРОВКА
    var flTop = 0;
    let arr = [];
    let idArrTop = 0;
    let ordiniHtmlText;
    let once = 0;

    //сортировка по ORDINAMENTO //не IDBOX
    let obj = {}
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            const el = RIGHE[key];
            const n = el.ORDINAMENTO//el.IDBOX
            if (!obj[n]) obj[n] = {}

            if (RIGHE.getTag(el, "KORPUSNAME")) {
                obj[n]["0"] = el
            } else {
                obj[n][key] = el
            }
        }
    }
    let htmltextDlin = "";
    //console.log(obj);
    window.printRighe = obj;


    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            const el = obj[key]
            for (const jey in el) {
                if (el.hasOwnProperty(jey) && el[jey].COD !== "FORTOP") {
                    const item = el[jey];

                    //длинномер
                    if ((item.ORDINAMENTO === '9999' || item.ORDINAMENTO === 9999)
                        || typeof (item.ORDINAMENTO) === 'undefined') {
                        if (flTop == 0) {
                            htmltextDlin += '<tr>\
                                <td class="tdBold">Длинномеры:</td>\
                                <td></td>\
                                <td></td>\
                                </tr>';
                            flTop = 1;
                        }
                        htmltextDlin += '<tr>\
                            <td>'+ item.MY_DES + '</td>\
                            <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
                            <td class="txtCentr">'+ item.PZ + '</td>\
                            </tr>'

                    } else {
                        //Примечание
                        if ((item.ORDINAMENTO !== '9999' && item.ORDINAMENTO !== 9999) && (Note && Note !== "")) {
                            htmltext += '<tr>\
                            <td class="tdBold">Примечание: '+ Note + '</td>\
                            <td></td>\
                            <td></td>\
                            </tr>';
                            Note = "";
                        }
                        //Позиция для корпусов
                        if (RIGHE.getTag(item, "KORPUSNAME")) {
                            htmltext += '<tr>\
                                <td class="tdBold">Позиция '+ item.ORDINAMENTO + '</td>\
                                <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
                                <td class="txtCentr">'+ item.PZ + '</td>\
                                </tr>';
                            Note = item.NOTE;

                        } else {
                            //Позиция для не корпусов
                            if (OrdinaOld != item.ORDINAMENTO) {
                                htmltext += '<tr>\
                                    <td class="tdBold">Позиция '+ item.ORDINAMENTO + '</td>\
                                    <td class="txtCentr"></td>\
                                    <td class="txtCentr"></td>\
                                    </tr>';
                            }

                            //остальные
                            htmltext += '<tr>\
                                <td>'+ item.MY_DES + '</td>\
                                <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
                                <td class="txtCentr">'+ item.PZ + '</td>\
                                </tr>'
                        }
                    }
                    OrdinaOld = item.ORDINAMENTO;
                }
            }
        }
    }
    htmltext += htmltextDlin;

    if (PREV.TESTA.VAR.NOTE && PREV.TESTA.VAR.NOTE !== "") {
        htmltext += '<tr>\
			  <td class="tdBold">Примечание: '+ PREV.TESTA.VAR.NOTE + '</td>\
			  <td></td>\
			  <td></td>\
		     </tr>';
    }
    htmltext += '</table></div>';
    htmltext += '<div class="ftrSpec">Перечень позиций представляет собой пример сборки из комплектующих, не является спецификацией к договору, не отменяет и не изменяет Спецификацию.</div>'
    elm.html(htmltext);
}