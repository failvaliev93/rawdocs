﻿/*
    Вспомогательные функции
*/
import X2JS from '@js/xml2json.js'
import * as $ from 'jquery'
//import jsonPrev from '@index.js'

var x2js = new X2JS();

//Преобразуем строку варинтов в массив [ключ,значение]
export function stringToMap(str) {
    return str.split(';').map(function (item) {
        return item.split('=');
    });
}

//Поиск ближайшего большего
export function upInt(arr, a) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] >= a) {
            var res = arr[i];
            return res
        }
    }
}

//для сортировки чисел в массиве начиная с меньшего
export function compareNumeric(a, b) {
    a = +a;
    b = +b;
    if (a > b) return 1;
    if (a == b) return 0;
    if (a < b) return -1;
}
//для сортировки чисел в массиве начиная с большего
export function compareNumericRev(a, b) {
    a = +a;
    b = +b;
    if (a < b) return 1;
    if (a == b) return 0;
    if (a > b) return -1;
}

export function compareNumbers(a, b) {
    return a - b;
}

export function sayHi() {
    console.log("Hi");
}

export function SaveXml() {
    var xml = '<?xml version="1.0"?>' + x2js.json2xml_str(jsonPrev);
    //xml = encodeCP1251(xml);
    var blob = new Blob([xml], { type: "text/plain" });
    saveAs(blob, "express.xml");
}

export function SaveFile(jsonPrev, event) {
    //console.log("SaveFile");
    event.target.innerText = "Сохранён"
    event.target.disabled = true

    //delete VARS
    for (const key in jsonPrev.PREV.RIGHE) {
        if (jsonPrev.PREV.RIGHE.hasOwnProperty(key)) {
            const el = jsonPrev.PREV.RIGHE[key];
            delete el.VARS
            delete el.VarArr
            let tempL = el.A;
            let tempA = el.L;
            let tempP = el.P;
            if (el.COD === "STP" || el.COD === "BOISERIE" || el.COD === "METAL" || el.COD === "TOP"
                || el.COD === "BAR_FOOT" || el.NEUTRO === "M1T" || el.NEUTRO === "PN60132-NEST"
                || el.NEUTRO === "PN60204-NEST" || el.NEUTRO === "M1S" || el.NEUTRO === "SMT"
                || el.COD === "KROMKA" || el.NEUTRO === "TUG12072-1F-N" || el.COD === "TOPUP"
                || el.NEUTRO === "M1S2" || el.NEUTRO === "M1T2") {
                //не меняю размеры
                //el.L = tempA;
                //el.A = tempL;
            } else {
                //меняю местами размеры
                el.A = el.L;
                el.L = tempL;
            }
        }
    }

    var xml = '<?xml version="1.0"?>' + x2js.json2xml_str(jsonPrev);
    //console.log(xml);
    var data = {
        expresshtml: b64EncodeUnicode(document.documentElement.innerHTML),
        expressxml: b64EncodeUnicode(xml)
    };
    _wsport = _wsport === "" ? "51278" : _wsport;

    var parmstr = b64EncodeUnicode(document.documentElement.innerHTML) + ";" + b64EncodeUnicode(xml);

    $.ajax({
        url: "http://localhost:" + _wsport,
        type: "POST",
        /*data: JSON.stringify(data),*/
        data: parmstr,
        success: function (data) { },
        error: function (data) { }
    });
}

export function preSave(jsonPrev) {
    //copy 
    let newjsonPrev = _.cloneDeep(jsonPrev);

    //delete VARS
    for (const key in newjsonPrev.PREV.RIGHE) {
        if (newjsonPrev.PREV.RIGHE.hasOwnProperty(key)) {
            const el = newjsonPrev.PREV.RIGHE[key];
            delete el.VARS
            delete el.VarArr
            let tempL = el.A;
            let tempA = el.L;
            let tempP = el.P;
            if (el.COD === "STP" || el.COD === "BOISERIE" || el.COD === "METAL" || el.COD === "TOP"
                || el.COD === "BAR_FOOT" || el.NEUTRO === "M1T" || el.NEUTRO === "PN60132-NEST"
                || el.NEUTRO === "PN60204-NEST" || el.NEUTRO === "M1S" || el.NEUTRO === "SMT"
                || el.COD === "KROMKA" || el.NEUTRO === "TUG12072-1F-N" || el.COD === "TOPUP"
                || el.NEUTRO === "M1S2" || el.NEUTRO === "M1T2") {
                //не меняю размеры
                //el.L = tempA;
                //el.A = tempL;
            } else {
                //меняю местами размеры
                el.A = el.L;
                el.L = tempL;
            }
        }
    }
    return newjsonPrev;
}

export function b64EncodeUnicode(str) {
    return btoa(
        encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(
            match,
            p1
        ) {
            return String.fromCharCode("0x" + p1);
        })
    );
}

//расчет количества кусков, простой
//arrL - массив длин, sumLReal - сумма длин, treshold - порог для объед-я 2х минимальных
//return [arr[count, L]]
export function calcOfPieces(arrL = [], sumLReal = 0, treshold = 0) {
    let res = [];

    if (arrL.length > 0 && sumLReal > 0) {
        arrL.sort(compareNumeric); //начиная с меньшего
        //иниц-я кол-ва кусков
        for (let i = 0; i < arrL.length; i++) {
            res.push([0, +arrL[i]]);
        }

        for (let i = arrL.length - 1; i >= 0; i--) {
            if (+arrL[i] <= sumLReal) { //кусок влезает
                let countI = Math.floor(sumLReal / arrL[i]);
                sumLReal -= +arrL[i] * countI;
                res[i][0] = countI;
            }
        }
        if (sumLReal > 0) { //остался кусочек 
            res[0][0] += 1;
        }

        //если 2 по 2000 умещаются в 4000
        //treshold=350: 2 по 1825 умещаются в 4000
        for (let i = 0; i < res.length; i++) {
            const min = res[i];
            if (min[0] > 1) {
                for (let j = 0; j < res.length; j++) {
                    const max = res[j];
                    if (min[1] === max[1]) continue; //сам на себя

                    let conditionCompare = min[0] > 0 && min[1] * 2 === max[1];
                    //treshold работает только для без излишеств, в меньшую сторону
                    if (treshold && min[1] * 2 < max[1]) {
                        conditionCompare = min[0] > 0 && (min[1] * 2 <= (max[1] - treshold));
                    }

                    if (conditionCompare) {
                        ++max[0];
                        --min[0];
                        --min[0];
                    }
                }
            }
        }

        //Infinity
        /*for (let i = 0; i < res.length; i++) {
            for (let j = 0; j < res[i].length; j++) {
                const el = res[i][j];
                if (isFinite(!el)) res[i][j] = 0;
            }
        }*/
        return res
    }
    return []
}

//"F*100*200" => [100, 200]
export function sizesConvertToArray(string) {
    let strArr = string.toUpperCase().split("*");

    //проверка вервого символа
    if (strArr[0] === 'S' || strArr[0] === 'F') strArr.shift();

    return strArr.map(item => { return +item });
}

//true если четная
export const even = n => !(n % 2);