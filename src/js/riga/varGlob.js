/* 
    Глобальные варианты из проекта
*/

export default class VarGlob {
    constructor(jsonPrev) {
        //console.log(jsonPrev.PREV.TESTA.VARGLOB); //del #gene.erg,
        this.VARS = jsonPrev.PREV.RIGHE.setVars(jsonPrev.PREV.TESTA.VARGLOB.substr(10), ',');
    }

    //return string
    getTag(tag) {
        if (!this.VARS) {
            console.log(tag, "--riga.VARS отсутствует");
            return ""
        } else {
            tag = tag.toString();
            for (let index = 0; index < this.VARS.length; index++) {
                const el = this.VARS[index];
                if (el[0] == tag) return el[1];
            }
            return ""
        }
    }
}