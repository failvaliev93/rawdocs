/*
    Операции над riga
*/

import {
    stringToMap,
    upInt,
    compareNumeric,
    compareNumbers,
    calcOfPieces,
    even,
    sizesConvertToArray
} from '@js/func.js';
import VarGlob from '@js/riga/varGlob.js';

let jsonPrev;
let varGlob;

export function rigaFunc(RIGHE, jsonPrev_) {
    jsonPrev = jsonPrev_;
    varGlob = new VarGlob(jsonPrev);

    getDlin(RIGHE, 'PLINTUS', "PLINT_COUNT", "");
    getDlin(RIGHE, 'SOCLE', "SOCLE_COUNT_MIN"); //, "SOCLE_COUNT_MAX"
    getDlin(RIGHE, 'KARNIZ', "KARNIZ_COUNT", "");
    getDlin(RIGHE, 'KARNIZN', "KARNIZN_COUNT", "");
    //getDlin(RIGHE, 'BALUSTRADA', "BALUSTRADA_COUNT", "");

    tableTop(RIGHE);
    stenpan(RIGHE);

    soclePostProc(RIGHE); //Цоколя
    balustPostProc(RIGHE); //балюстрады

    lightPostProc(RIGHE); //свет
    tubePostProc(RIGHE); //опора
    difItem(RIGHE);

    postJsonPrev(RIGHE);
}

//для длинномеров
//  вытаскивает из detpresso findCod findCod2 и
//  копирует в новую riga оттуда кол-во и все данные из родного cod и удаляет cod.
//  так же из detpresso вытаскивает все остальные riga
function getDlin(RIGHE, cod, findCod, findCod2) {
    var arrRiga = [];
    var rigaIndex = 0;

    let highIDBOX = 0;
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            if (highIDBOX < RIGHE[key].IDBOX) highIDBOX = RIGHE[key].IDBOX
        }
    }

    //Выбираем только плинтус
    var plint = _.filter(RIGHE, { 'COD': cod });
    //if (cod == 'KARNIZ') console.log("plint", plint);

    plint.forEach(function (item) {
        //Читаем спецификацию цены
        //console.log(item.IDBOX, item);

        //IDBOX меняю !!!
        highIDBOX++;
        item.IDBOX = highIDBOX;

        let rDetpresso = item.DETTPREZZO.split("#");
        rDetpresso = _.drop(rDetpresso, 1);//удаляет первый элемент
        rDetpresso.forEach(function (r) {
            let s = [];
            s = r.split(";");
            //чувак без вариантов или одинаковым вариантом //сам длинномер
            if (s[1] === findCod || s[1] === findCod2) {
                let riga = RIGHE.createBased(item);//CreateRiga();
                riga.COD = cod;
                riga.L = +s[7].split(',')[1];
                riga.PZ = +s[6];
                riga.MY_REAL_L = +item.L; //Дописываю +свойство реальный размер
                riga.MY_UNI_ID = RIGHE.setUniId();
                arrRiga.push(riga);
            } else {
                //чувак с личным вариантом //элементы длинномера
                let varVar = "";
                if (s.length >= 8) {
                    for (let i = 8; i < s.length; i++) { //пересобираю варианты для элемента
                        s[i] = s[i].split("=")[0].toUpperCase() + "=" + s[i].split("=")[1];//делаю большими буквами варианты
                        if (i === s.length - 1) {
                            varVar += s[i];
                        } else {
                            varVar += s[i] + ";";
                        }
                    }
                }
                varVar = varVar + ";";
                let riga = RIGHE.create();//RIGHE.createBased(item);//CreateRiga();
                riga.COD = s[1];
                if (typeof s[7] === 'undefined') { riga.L = 0 }
                else { riga.L = s[7].split(',')[1]; }
                riga.PZ = +s[6];
                riga.A = item.A;
                riga.P = item.P;
                riga.VAR = varVar;
                riga.VARS = RIGHE.setVars(varVar);
                riga.IDBOX = item.IDBOX;
                riga.ORDINAMENTO = item.ORDINAMENTO;
                riga.MY_DES = RIGHE.getTag(riga, "MYNAME");
                arrRiga.push(riga);
            }
        });
    });
    //Дописываю в описание длину
    for (let i = 0; i < arrRiga.length; i++) {
        if (arrRiga[i].COD === cod) {
            let kk = RIGHE.getTag(arrRiga[i], "MYNAME");
            RIGHE.setTag(arrRiga[i], "MYNAME", kk + ", Д" + arrRiga[i].L);
        }
    }
    //console.log(cod, arrRiga);

    //Добавляем новые строки и удаляем старые
    if (arrRiga.length > 0) {
        //Удаляем плинтус 
        //for (let key in RIGHE) {
        //Номер строки 
        //rigaIndex = _.parseInt(key.replace('RIGA', ''));
        /*if (RIGHE[key].COD === cod) {
            delete RIGHE[key];
        }*/
        //}
        //rigaIndex = _.parseInt(key.replace('RIGA', ''));
        //if (cod == 'KARNIZ') debugger;
        RIGHE.delByCod(cod);

        //rigaIndex++;
        rigaIndex = 1 + RIGHE.lastNumber();

        arrRiga.forEach(function (r) {
            //RIGHE['RIGA' + rigaIndex] = r;
            //if (cod == 'KARNIZ') console.log(r);
            RIGHE.addNewToEnd(r);
            //RIGHE.addNew(r, rigaIndex)
            //rigaIndex++;
        });
    }

    //ПОСТОБРАБОТКА
    //карнизов верхних
    if (cod == 'KARNIZ') {
        karnizPostProc(arrRiga, RIGHE);

    } else if (cod == 'KARNIZN') {
        //карнизов нижних
        karnizNPostProc(arrRiga, RIGHE);

    } else if (cod == 'PLINTUS') {
        //плинтусов
        plintusPostProc(arrRiga, RIGHE);
    }
}

//Постобработка цоколя
function soclePostProc(RIGHE) {
    let groupVars = ["SOCLE_PRODUCT", "IZGOTOV", "SOCLE_COLOR", "H_SOCLE", "SOCLE_RADIUS"];
    //console.log(arrRiga);
    /*for (let i = 0; i < arrRiga.length; i++) {
        if (arrRiga[i].COD === "SOCLE") {
            //беру значение тега
            let myarticle = RIGHE.getTag(arrRiga[i], "ARTICLE");
            myarticle = myarticle + '\\' + arrRiga[i].L / 1000 + "m"; // \
            arrRiga[i].VAR = arrRiga[i].VAR + ";MYARTICLE=" + myarticle;//и добавляю в варианты
            arrRiga[i].VAR = arrRiga[i].VAR + ";SOCLE_ART=" + myarticle;
            arrRiga[i].MY_DES += `. Д${arrRiga[i].L}`;
        }
    }*/
    //для 180 загл всегда -1
    let group = RIGHE.group(groupVars, 'SOCLE');
    let realAllLength = 0;
    const tresholdCompare = 350;

    if (!_.isEmpty(group)) {
        //предварительно длины радиусных цоколей добавляю к прямым
        //точно знаем, что сортируются по SOCLE_RADIUS
        let addLength = 0;
        for (const key in group) {
            if (!_.isEmpty(group[key])) {
                const SOCLE_RADIUS = RIGHE.getTag(group[key][0], "SOCLE_RADIUS");
                if (SOCLE_RADIUS) {
                    for (const jey in group[key]) {
                        addLength += group[key][jey].MY_REAL_L;
                    }
                }
            }
        }

        //обработка цоколей
        for (const key in group) {
            let length = 0; //общая длина
            let first = ""; //ID

            if (!_.isEmpty(group[key])) {
                //исключаю радиусных из-за наличия тега SOCLE_RADIUS
                const SOCLE_RADIUS = RIGHE.getTag(group[key][0], "SOCLE_RADIUS");

                //обработка радиусных
                if (SOCLE_RADIUS) {
                    for (const jey in group[key]) {
                        const el = group[key][jey];
                        el.L = 503;
                    }

                } else { //прямые
                    length = addLength;
                    addLength = 0; // сброс после первого
                    for (const jey in group[key]) {
                        const el = group[key][jey];
                        length += el.MY_REAL_L;

                        //кроме первого
                        if (first) {
                            RIGHE.delByUniId(el.MY_UNI_ID);
                        } else {
                            first = el.MY_UNI_ID;
                        }
                    }
                    let riga = RIGHE.findByUniId(first);
                    let pieces = RIGHE.getTag(riga, 'SIZES'); //SOCLE_L1 устарело
                    let pvh = RIGHE.getTag(riga, 'MANUF');

                    let res = calcOfPieces(sizesConvertToArray(pieces), length, tresholdCompare);

                    for (let i = 0; i < res.length; i++) {
                        const el = res[i];
                        //есть кусок
                        if (el[0] > 0) {
                            //записываю на родной riga
                            if (first) {
                                riga.L = el[1];
                                riga.PZ = el[0];
                                riga.MY_DES += `. Д${riga.L}`;
                                first = "";

                                if (pvh === "PVH") RIGHE.setTag(riga, "ARTICLE", RIGHE.getTag(riga, "ARTICLE") + '\\' + riga.L / 1000 + "m");
                                RIGHE.newTag(riga, "MYARTICLE", RIGHE.getTag(riga, "ARTICLE"));
                                RIGHE.newTag(riga, "SOCLE_ART", RIGHE.getTag(riga, "ARTICLE"));

                            } else {
                                //создаю еще
                                //debugger
                                let newRiga = RIGHE.createBased(riga);
                                newRiga.MY_UNI_ID = RIGHE.setUniId();
                                newRiga.L = el[1];
                                newRiga.PZ = el[0];
                                newRiga.MY_DES = riga.MY_DES.replace(`Д${riga.L}`, `Д${newRiga.L}`);
                                //if (pvh === "PVH") RIGHE.setTag(newRiga, "ARTICLE", RIGHE.getTag(newRiga, "ARTICLE") + '\\' + newRiga.L / 1000 + "m");

                                //art.replace(`${riga.L / 1000}m`, `${newRiga.L / 1000}m`);
                                //art.replace(riga.L / 1000 + "m".toString(), newRiga.L / 1000 + "m".toString());

                                if (pvh === "PVH") {
                                    let art = RIGHE.getTag(riga, "MYARTICLE");
                                    art = art.split(riga.L / 1000 + "m")[0] + newRiga.L / 1000 + "m";
                                    RIGHE.setTag(newRiga, "ARTICLE", art);
                                    RIGHE.setTag(newRiga, "MYARTICLE", art);
                                    RIGHE.setTag(newRiga, "SOCLE_ART", art);
                                }
                                RIGHE.addNewToEnd(newRiga);
                            }
                        }
                    }

                    realAllLength += length; //вся нарисованная длина
                }
            }
        }
    }

    //собираю заново группу, т.к. она изменилась //сомнительно
    group = RIGHE.group(groupVars, 'SOCLE');


    //Simple Line
    let sealRef;
    //оставляю только одну в sealRef удаляя SEAL
    for (const key in RIGHE) {
        if (Object.hasOwnProperty.call(RIGHE, key)) {
            const el = RIGHE[key];
            if (el.COD === "SEAL") {
                if (!sealRef) sealRef = el;
                else delete RIGHE[key];
            }
        }
    }
    if (realAllLength && sealRef) {
        sealRef.QTA = sealRef.PZ = Math.ceil(realAllLength / 5000);
        RIGHE.setChildByCod(sealRef, "SOCLE");
    }


    //водоотталкивающий(профиль)
    let length = 0;
    let lastSocle;
    if (!_.isEmpty(group)) {
        for (const key in group) {
            if (!_.isEmpty(group[key])) {
                for (const jey in group[key]) {
                    let el = group[key][jey];
                    const SOCLE_COD = RIGHE.getTag(el, 'SOCLE_COD');
                    if (SOCLE_COD === '01' || SOCLE_COD === '02.02') {
                        length += el.MY_REAL_L;
                        lastSocle = el;
                    }
                }
            }
        }
    }
    if (length && !sealRef && lastSocle) {
        let riga = RIGHE.create();
        riga.COD = 'PROFW';
        riga.L = 3000;
        riga.A = 16;
        riga.PZ = riga.QTA = Math.ceil(length / 3000);
        riga.MY_DES = "Профиль водоотталкивающий для ЛДСП 3000х16мм";
        riga.IDBOX = lastSocle.IDBOX;
        RIGHE.newTag(riga, 'MYARTICLE', 'prof_water_16vv');
        RIGHE.newTag(riga, 'PROFW_ART', 'prof_water_16vv');
        RIGHE.addNewToEnd(riga);
    }
}

//Постобработка карнизов верхних
function karnizPostProc(arrRiga, RIGHE) {
    //убрал из проги подсчет кол-ва, дабы потом ваще его убрать из getDlin
    //собираю одинаковые карнизы в 1
    let group = RIGHE.group([
        "KARN_PRODUCT",
        "IZGOT",
        "KARN_COLOR",
        "FREZ",
        "PATINA",
        "POVERHNOST",
        "SPECEFFECT",
        "KARNIZ_TYPE"
    ], 'KARNIZ');

    if (!_.isEmpty(group)) {
        for (const key in group) {
            let length = 0; //общая длина
            let first = ""; //ID

            if (!_.isEmpty(group[key])) {
                for (const jey in group[key]) {
                    const el = group[key][jey];
                    length += el.MY_REAL_L;

                    let izgot = RIGHE.getTag(el, "IZGOT");
                    let frez = RIGHE.getTag(el, "FREZ");
                    let karn_product = RIGHE.getTag(el, "KARN_PRODUCT");
                    let gnut = RIGHE.getTag(el, "KARNIZ_TYPE");

                    const KARN_A = +RIGHE.getTag(el, "KARN_A");

                    //высоту получаю из каталога
                    if (KARN_A && !gnut) {
                        el.A = +KARN_A;
                    } else if (izgot && !gnut) {
                        if (izgot === "ВТС" && frez === "К3") el.A = 120;
                        if (izgot === "ВТС" && frez === "К4") el.A = 100;
                        if (izgot === "ВТС" && frez === "К6") el.A = 50;
                        if (izgot === "ВТС" && frez === "К7") el.A = 100;
                        if (izgot === "ВТС" && frez === "К8") el.A = 100;
                        if (izgot === "Сидак" && karn_product === "Карниз верхний 1+") el.A = 100;
                        if (izgot === "Сидак" && karn_product === "Карниз верхний 2+") el.A = 90;
                        if (izgot === "Фабриче" && frez === "№1") el.A = 33;
                        if (izgot === "Фабриче" && frez === "№2") el.A = 44;
                        if (izgot === "Фабриче" && frez === "№3") el.A = 44;
                        if (izgot === "Бином") el.A = 41;
                        if (izgot === "Contour") el.A = 99;
                        if (izgot === "Выбор") el.A = 41;
                    } else if (izgot && gnut) {
                        if (izgot === "Бином") el.A = el.L = 396;
                        if (izgot === "ВТС" && frez === "К4") el.A = 100; el.L = 503;
                        if (izgot === "ВТС" && frez === "К7") el.A = 100; el.L = 503;
                        if (izgot === "Сидак") el.A = el.L = 410;
                        if (izgot === "Фабриче") el.A = 33; el.L = 503;
                        if (izgot === "Выбор") el.A = 41;
                        el.PZ = 1;
                    }

                    //удаляю кроме первого
                    if (first) {
                        if (!gnut) {
                            RIGHE.delByUniId(el.MY_UNI_ID);
                        }
                    } else {
                        first = el.MY_UNI_ID;
                    }
                }
                let riga = RIGHE.findByUniId(first);
                let piece = +RIGHE.getTag(riga, 'KARN_L1');
                let gnut = RIGHE.getTag(riga, "KARNIZ_TYPE");
                riga.L = piece;
                riga.MY_DES += `. Д${riga.L}`;

                if (gnut) {
                    //riga.PZ = forceCount;
                } else {
                    if (piece < length) {
                        let count = Math.ceil(length / piece);
                        riga.PZ = count;
                        //console.log(length, piece, count);
                    } else {
                        riga.PZ = 1;
                    }
                }
                //if (gnut) riga.PZ = 1;
            }
        }
    }

    return arrRiga;
}

//Постобработка карнизов нижних
function karnizNPostProc(arrRiga, RIGHE) {
    for (let i = 0; i < arrRiga.length; i++) {
        if (arrRiga[i].COD === 'KARNIZN') {
            arrRiga[i].MY_DES += `. Д${arrRiga[i].L}`;
        }
    }

    //собираю одинаковые карнизы в 1
    let group = RIGHE.group(["KARN_PRODUCT",
        "IZGOT",
        "KARN_COLOR",
        "FREZ",
        "PATINA",
        "POVERHNOST",
        "SPECEFFECT",
        "KARNIZ_TYPE"
    ], 'KARNIZN');
    //console.log(group);

    if (!_.isEmpty(group)) {
        for (const key in group) {
            let length = 0;
            let first = "";

            if (!_.isEmpty(group[key])) {
                for (const jey in group[key]) {
                    const el = group[key][jey];
                    length += el.MY_REAL_L;

                    let izgot = RIGHE.getTag(el, "IZGOT");
                    let frez = RIGHE.getTag(el, "FREZ");
                    let karn_product = RIGHE.getTag(el, "KARN_PRODUCT");
                    let gnut = RIGHE.getTag(el, "KARNIZ_TYPE");

                    if (izgot && !gnut) {
                        if (izgot === "ВТС") el.A = 50;
                        if (izgot === "Сидак") el.A = 55;
                        if (izgot === "Бином") el.A = 55;
                        if (izgot === "Contour" && karn_product === "Карниз нижний 510") el.A = 56;
                        if (izgot === "Contour" && karn_product === "Карниз нижний 532") el.A = 57;
                        if (izgot === "Contour" && karn_product === "Карниз нижний 519") el.A = 35;
                    } else if (izgot && gnut) { //izgot & 
                        if (izgot === "Бином") el.A = 55; el.L = 319;
                        if (izgot === "ВТС") el.A = 50; el.L = 503;
                        if (izgot === "Сидак") el.A = 55; el.L = 421;
                        el.PZ = 1;
                    }

                    //кроме первого
                    if (first) {
                        if (!gnut) {
                            RIGHE.delByUniId(el.MY_UNI_ID);
                        }
                    } else {
                        first = el.MY_UNI_ID;
                    }
                }
                let riga = RIGHE.findByUniId(first);
                let piece = +RIGHE.getTag(riga, 'KARN_L1');
                let gnut = RIGHE.getTag(riga, "KARNIZ_TYPE");

                if (gnut) {
                    //riga.PZ = 1;
                } else {
                    if (piece < length) {
                        let count = Math.ceil(length / piece);
                        riga.PZ = count;
                        //console.log(length, piece, count);
                    } else {
                        riga.PZ = 1;
                    }
                }
            }
        }

    }
    return arrRiga;
}

//Постобработка плинтусов
function plintusPostProc(arrRiga, RIGHE) {
    for (let i = 0; i < arrRiga.length; i++) {
        if (arrRiga[i].COD === 'PLINTUS') {
            arrRiga[i].MY_DES += `. Д${arrRiga[i].L}`;
        }
    }
    //собираю одинаковые плинтуса в 1
    let group = RIGHE.group(["PLINT_PRODUCT", "TYPE_OF_PLINT", "PLINT_COLOR"], 'PLINTUS');

    if (!_.isEmpty(group)) {
        for (const key in group) {
            let length = 0; //общая длина
            let first = ""; //ID

            if (!_.isEmpty(group[key])) {
                for (const jey in group[key]) {
                    const el = group[key][jey];
                    length += el.MY_REAL_L;

                    //кроме первого
                    if (first) {
                        RIGHE.delByUniId(el.MY_UNI_ID);
                    } else {
                        first = el.MY_UNI_ID;
                    }
                }
                let riga = RIGHE.findByUniId(first);
                let piece = +RIGHE.getTag(riga, 'PLINT_L1');
                riga.L = piece;

                if (piece < length) {
                    let count = Math.ceil(length / piece);
                    riga.PZ = count;
                    //console.log(length, piece, count);
                } else {
                    riga.PZ = 1;
                }
            }
        }
    }

    //собираю одинаковые плинтуса в 1
    group = RIGHE.group(["ZAGL_ART"], 'ZAGL');

    if (!_.isEmpty(group)) {
        for (const key in group) {
            let count = 0; //общая кол-во
            let first = ""; //ID

            if (!_.isEmpty(group[key])) {
                for (const jey in group[key]) {
                    const el = group[key][jey];
                    count += +el.PZ;

                    //кроме первого
                    if (first) {
                        RIGHE.delByUniId(el.MY_UNI_ID);
                    } else {
                        first = el.MY_UNI_ID;
                    }
                }
                let riga = RIGHE.findByUniId(first);
                riga.PZ = count;
            }
        }
    }
    return arrRiga
}

//постобработка JsonPrev
function postJsonPrev(RIGHE) {
    //заменяю #nbsp на пробелы
    let del = []
    for (let key in RIGHE) {
        let item = RIGHE[key];
        //RIGHE[key];
        //console.log(key);
        for (let k in RIGHE[key]) {
            if (k === "DETTPREZZO" || k === "VAR") {
                let str = RIGHE[key][k];
                //console.log(k);
                str.replace(/\&nbsp\;/gi, ' ');
            }
        }

        //FORTOP удаляю
        const SERVICE = RIGHE.getTag(item, "SERVICE").toLowerCase();
        const SERVICE_MAN = RIGHE.getTag(item, "SERVICE_MAN").toLowerCase();
        if (SERVICE === "delete" || SERVICE_MAN === "delete") {
            del.push(RIGHE[key].MY_UNI_ID)
        }

        //безкорпусные тумбы
    }
    RIGHE.delByUniId(del)

    //удаляю WORK
    let ASSEMBLY = varGlob.getTag("ASSEMBLY");
    if (ASSEMBLY === "4") {
        RIGHE.delByCod(["WORK"]);
    }
    //console.log(ASSEMBLY);

    //удаление по коду DELETE
    RIGHE.delByCod(["DELETE"]);
}

//Обработка столешниц
function tableTop(RIGHE) {
    let forLentaWidth = 0; //сумма ленты
    let lentaCount = 1; //количество ленты
    let lentaHeight = 45; //высота ленты
    let lentaLength = 3050; //длина ленты
    let arrLenta = []; //список лент
    let arrLentaInCount = 0; //порядковый номер каждой ленты
    let highIDBOX = 0;
    let arrForTop = [];

    //let glueCount = 0;
    let glueCountOfTech = 0;
    let glueCountOfPlith = 0;

    //high IDBOX
    /*for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            if (highIDBOX < RIGHE[key].IDBOX) highIDBOX = RIGHE[key].IDBOX
        }
    }*/

    //перемещение столешек на свои места// !!! error
    /*let arrTopId = [];
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            if (RIGHE[key].COD == "FORTOP") {
                arrForTop.push(RIGHE[key].MY_UNI_ID);
            }
            if (RIGHE[key].COD == "TOP") {
                arrTopId.push(RIGHE[key].MY_UNI_ID);
            }
        }
    }
    for (let i = 0; i < arrForTop.length; i++) {
        let riga = RIGHE.findByUniId(arrTopId[i]);
        RIGHE.replaceByUniId(riga, arrForTop[i]);
    }*/

    //собираю длину //ЛЕНТА КРОМКА
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            const el = RIGHE[key];
            if (el.COD == "TOP") {
                //IDBOX меняю !!!
                //highIDBOX++;
                //el.IDBOX = highIDBOX;

                //размеры столешки
                const REAL_L = RIGHE.getTag(el, 'REAL_L');
                //if (el.L == 1500) el.L += 30;
                el.MY_REAL_L = REAL_L ? +REAL_L : el.L;

                //к столешнице присваиваю L
                var top_l = RIGHE.getTag(el, 'TOP_L'); //список длин
                var fulltop = _.union(fulltop, top_l.split('*').map(function (item) { return Number(item) }));
                el.L = upInt(fulltop, el.L); //ближайшего большего
                let top_find_l = +RIGHE.getTag(el, "TOP_FIND_L");

                //перевожу L к 1С
                el.L = topLOut(top_find_l, el, RIGHE);
                el.MY_REAL_A = el.A; //для подсчета клея
                el.A = topPOut(el.A, el, RIGHE); //глубина
                el.MY_DES = el.MY_DES + el.L + ". Ш" + el.A;

                //собираю длину//ЛЕНТА КРОМКА 
                if (RIGHE.getTag(el, "KROMKA_L_COD") === "12") forLentaWidth = +el.A;
                if (RIGHE.getTag(el, "KROMKA_R_COD") === "12") forLentaWidth = +el.A + forLentaWidth;
                if (RIGHE.getTag(el, "KROMKA_Z_COD") === "12") forLentaWidth = el.MY_REAL_L + forLentaWidth;
                if (RIGHE.getTag(el, "KROMKA_COD") === "12") forLentaWidth = el.MY_REAL_L + forLentaWidth;

                //у барной другая форма
                const RADIUS = RIGHE.getTag(el, "RADIUS")
                if (RADIUS === "Радиусная барная стойка") {
                    if (RIGHE.getTag(el, "KROMKA_L_COD") === "12") forLentaWidth = el.MY_REAL_L;
                    if (RIGHE.getTag(el, "KROMKA_R_COD") === "12") forLentaWidth += el.MY_REAL_L;
                    if (RIGHE.getTag(el, "KROMKA_Z_COD") === "12") forLentaWidth += +el.A;
                    if (RIGHE.getTag(el, "KROMKA_COD") === "12") forLentaWidth += +el.A;
                }
                //console.log(forLentaWidth);

                if (forLentaWidth) {
                    let manufacturer = RIGHE.getTag(el, "TOP_MANUFACTURER");
                    let stolesh_mat = RIGHE.getTag(el, "STOLESH_MAT");
                    let stolesh_height = el.P;
                    //console.log(arrLentaInCount, manufacturer, stolesh_mat, stolesh_height, el.IDBOX, el.IDUNICO, el.ORDINAMENTO);

                    if (arrLenta.length == 0) {
                        arrLenta.push([
                            forLentaWidth,
                            manufacturer,
                            stolesh_mat,
                            stolesh_height,
                            el.IDBOX,
                            el.IDUNICO,
                            el.ORDINAMENTO,
                            el.MY_UNI_ID
                        ])
                    } else {
                        let find = false;

                        for (let i = 0; i < arrLenta.length; i++) {
                            if (manufacturer == arrLenta[i][1] &&
                                stolesh_mat == arrLenta[i][2] &&
                                stolesh_height == arrLenta[i][3]) {
                                arrLenta[i][0] += forLentaWidth;
                                arrLenta[i][4] = el.IDBOX;
                                arrLenta[i][5] = el.IDUNICO;
                                arrLenta[i][6] = el.ORDINAMENTO;
                                arrLenta[i][7] = el.MY_UNI_ID;
                                find = true;
                                break;
                            }
                        }
                        if (!find) {
                            arrLenta.push([
                                forLentaWidth,
                                manufacturer,
                                stolesh_mat,
                                stolesh_height,
                                el.IDBOX,
                                el.IDUNICO,
                                el.ORDINAMENTO,
                                el.MY_UNI_ID
                            ])
                        }
                    }
                    arrLentaInCount++;
                    forLentaWidth = 0;
                }

                //КЛЕЙ
                //наличие плинтуса
                //if (RIGHE.getTag(el, "MYARTICLE") === "workstone1") glueCountOfPlith++;
                if (RIGHE.getTag(el, "IS_PLINTH") === "1") glueCountOfPlith = 1;

            } else if (el.COD === "WORK") {
                // от работы по вырезу
                //if (RIGHE.getTag(el, "IS_PLINTH") === "1") glueCountOfTech++;
                if (RIGHE.getTag(el, "MYARTICLE") === "workstone1") glueCountOfTech++;
            }
        }
    }
    //console.log(arrLenta);


    //РАБОТА Изготовление кам стол
    let topStoneWidth = 0;
    let lastTopStone;
    let topStoneWidthAbove600 = 0; //у кого глубина выше 600

    //собираю длину каменных
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            const el = RIGHE[key];
            if (el.COD == "TOP") {
                let TOP_MANUFACTURER = RIGHE.getTag(el, "TOP_MANUFACTURER");
                if (TOP_MANUFACTURER === "SUMSUNG" || TOP_MANUFACTURER === "HIMACS" || TOP_MANUFACTURER === "GRANDEX") {
                    topStoneWidth += el.MY_REAL_L;
                    lastTopStone = el;

                    //для работы глубина выше 600
                    if (el.MY_REAL_A > 760) topStoneWidthAbove600 += el.MY_REAL_L;
                }
            }
        }
    }

    if (topStoneWidth > 0) {
        let des = "", art = "";

        if (topStoneWidth <= 3500) {
            des = "Изготовление каменной столешницы (глубина 600мм) 1 лист (до 3.5пм)";
            art = "workstone12";
        } else if (topStoneWidth <= 4400) {
            des = "Изготовление каменной столешницы (глубина 600мм) 1.25 листа (от 3.5 до 4.4пм)";
            art = "workstone13";
        } else if (topStoneWidth <= 5300) {
            des = "Изготовление каменной столешницы (глубина 600мм) 1.5 листа (от 4.4 до 5.3пм)";
            art = "workstone14";
        } else if (topStoneWidth <= 6100) {
            des = "Изготовление каменной столешницы (глубина 600мм) 1.75 листа (от 5.3 до 6.1пм)";
            art = "workstone15";
        } else if (topStoneWidth <= 7000) {
            des = "Изготовление каменной столешницы (глубина 600мм) 2 листа (от 6.1 до 7.0пм)";
            art = "workstone16";
        } else if (topStoneWidth <= 7900) {
            des = "Изготовление каменной столешницы (глубина 600мм) 2.25 листа (от 7.0 до 7.9пм)";
            art = "workstone17";
        } else {
            //} else if (panelAllWidth < 8800) {
            des = "Изготовление каменной столешницы (глубина 600мм) 2.5 листа (от 7.9 до 8.8пм)";
            art = "workstone18";
        }

        let riga = RIGHE.create();
        riga.COD = "WORK";
        riga.IDBOX = lastTopStone.IDBOX; //к последнему
        riga.IDUNICO = lastTopStone.IDUNICO;
        riga.MODELLO = lastTopStone.MODELLO;
        riga.QTA = riga.PZ = 1;
        riga.MY_DES = des;
        riga.DETTPREZZO = riga.VAR = `MYNAME=${des};MYARTICLE=${art};WORK_ART=${art}`;
        riga.VARS = RIGHE.setVars(riga.VAR);
        RIGHE.addNewToEnd(riga);

        if (topStoneWidthAbove600 > 0) {
            des = "Склейка элементов столешниц при нестандартной глубине (более 600мм) 1 п.м.";
            art = "workstone28";
            let riga2 = RIGHE.create();
            riga2.IDBOX = lastTopStone.IDBOX; //к последнему
            riga2.IDUNICO = lastTopStone.IDUNICO;
            riga2.MODELLO = lastTopStone.MODELLO;
            riga2.COD = "WORK";
            riga2.QTA = riga2.PZ = Math.ceil((topStoneWidthAbove600 / 1000) / 2);
            riga2.MY_DES = des;
            RIGHE.newTag(riga2, "MYNAME", des);
            RIGHE.newTag(riga2, "WORK_ART", art);
            RIGHE.newTag(riga2, "MYARTICLE", art);
            RIGHE.addNewToEnd(riga2);
        }
    }


    //ЛЕНТА КРОМКА добавляю строчку
    //Объединяется декор, производитель, высота
    for (let i = 0; i < arrLenta.length; i++) {
        forLentaWidth = arrLenta[i][0];
        let manufacturer = arrLenta[i][1];
        let stolesh_mat = arrLenta[i][2];
        let stolesh_height = arrLenta[i][3];

        if (forLentaWidth > 0) {
            //считаю
            if (manufacturer === "KEDR" || manufacturer === "SOYUZ" || manufacturer === "Eclipse") {
                if (manufacturer === "SOYUZ") lentaHeight = 45;
                if (manufacturer === "KEDR") lentaHeight = 44;
                if (manufacturer === "Eclipse") lentaHeight = 32;
                if (forLentaWidth > 0) lentaCount = Math.ceil(forLentaWidth / 3000); //округляю в большую
            } else if (manufacturer === "SLOTEX") {
                lentaHeight = 45; //трепло 127
                if (forLentaWidth <= 3000) { } //стандартно
                else if (forLentaWidth <= 4150) lentaLength = 4200;
                else if (forLentaWidth > 4150) {
                    lentaCount = Math.ceil(forLentaWidth / 4150);
                    lentaLength = 4200;
                }
            }
            //записываю
            let newRiga = RIGHE.create();
            newRiga.COD = "KROMKA";
            newRiga.L = lentaLength;
            newRiga.A = lentaHeight;
            newRiga.P = "0.6";
            newRiga.PZ = lentaCount; //количечтво
            newRiga.VAR = "MYNAME=Лента кромка с клеем " + manufacturer + ", " + stolesh_mat + ", Д" + lentaLength + ", Ш" + newRiga.A + ";KROMKA_MAT=" + stolesh_mat + ";KROMKA_MANUFAKTURER=" + manufacturer;
            newRiga.VARS = RIGHE.setVars(newRiga.VAR);
            newRiga.MY_DES = RIGHE.getTag(newRiga, "MYNAME");
            newRiga.IDBOX = arrLenta[i][4];
            newRiga.IDUNICO = arrLenta[i][5];
            newRiga.ORDINAMENTO = arrLenta[i][6];
            //console.log(newRiga);
            RIGHE.addNewToEnd(newRiga);

            //погонаж работы worktable5
            // for (const key in RIGHE) {
            //     if (RIGHE.hasOwnProperty(key)) {
            //         if (RIGHE[key].COD === "WORK") {
            //             if (RIGHE.getTag(RIGHE[key], "MYARTICLE") === "worktable5") {
            //             }
            //         }
            //     }
            // }
            let work = RIGHE.create();
            work.COD = "WORK";
            work.PZ = work.QTA = Math.ceil(forLentaWidth / 1000);
            work.IDBOX = arrLenta[i][4];
            work.IDUNICO = arrLenta[i][5];
            work.ORDINAMENTO = arrLenta[i][6];
            work.MY_DES = "Может понадобиться Кромление стол по прямой без цены кромки (на складе)";
            RIGHE.newTag(work, "MYARTICLE", "worktable5");
            RIGHE.newTag(work, "WORK_ART", "worktable5");
            RIGHE.addNewToEnd(work);
        }
    }

    //АВТОПОДСЧЕТ КЛЕЯ
    //группирую
    let topGroupColor = RIGHE.group(["GLUE", "TOP_MANUFACTURER", "TOLSHINA"], "TOP");
    let glueGroupOfTop = RIGHE.group(["GLUE"], "GLUE");
    //console.log(topGroupColor);
    //console.log(glueGroupOfTop);

    //суммирую количества по группам
    if (!_.isEmpty(topGroupColor)) {
        for (const key in topGroupColor) {
            let glueCount = 0;
            let topL = 0; //сумма длин
            let lastTop;
            let TOP_MANUFACTURER = "";
            let TOLSHINA = 0;

            //к каждой столешке
            if (!_.isEmpty(topGroupColor[key])) {
                for (const jey in topGroupColor[key]) {
                    const el = topGroupColor[key][jey];
                    TOP_MANUFACTURER = RIGHE.getTag(el, "TOP_MANUFACTURER");
                    TOLSHINA = +RIGHE.getTag(el, "TOLSHINA");

                    if (TOP_MANUFACTURER === "HIMACS" || TOP_MANUFACTURER === "SUMSUNG" || TOP_MANUFACTURER === "GRANDEX") {
                        topL += +el.L;

                        //к 12 толщине другой расчёт
                        if (TOLSHINA == 12) {
                        } else {
                            // фигурным +1
                            if (RIGHE.getTag(el, "RADIUS") !== "Прямоуголная" &&
                                RIGHE.getTag(el, "RADIUS") !== "Прямоугольная") {
                                glueCount++;
                                //console.log(`за фигурные ${Math.ceil(topL / 3680)}`);
                            }
                            // ширина не 600 +1
                            //el.MY_REAL_A = 700;
                            //if ((+el.MY_REAL_A > 600)) {
                            //    glueCount++;
                            //    console.log(`за ширину +1`);
                            //}
                        }
                        lastTop = el;
                    }
                }
            }

            if (TOP_MANUFACTURER === "HIMACS" || TOP_MANUFACTURER === "SUMSUNG" || TOP_MANUFACTURER === "GRANDEX") {
                //к 12 толщине другой расчёт
                if (TOLSHINA == 12) {
                    //на каждые 3680мм столешки
                    glueCount += Math.ceil(topL / 3680);

                } else {
                    //на каждые 3000мм столешки
                    glueCount += Math.ceil(topL / 3680);
                    //console.log(`за 3м ${Math.ceil(topL / 3680)}`);

                    //сборный мойка, варка, плинтус
                    glueCount += glueCountOfTech + glueCountOfPlith;

                    //контрольный
                    glueCount++;
                }

                //ИЩЮ КЛЕЙ
                let topGlueCod = RIGHE.getTag(lastTop, "GLUE");
                let lastGlue;

                //при условии что тип клея одинаков для столешниц с одинаковым декором
                //удаляю клей только с одинаковым цветом
                if (!_.isEmpty(glueGroupOfTop)) {
                    for (const jey in glueGroupOfTop) {

                        if (!_.isEmpty(glueGroupOfTop[jey])) {
                            let isGlueHandleCountAboveZero = false;

                            //проверка на ручной ввод >0 для всей коллекции
                            for (const mey in glueGroupOfTop[jey]) {
                                const el = glueGroupOfTop[jey][mey];
                                let glueHandleCount = +RIGHE.getTag(el, "GLUE_COUNT");
                                glueHandleCount > 0 ? isGlueHandleCountAboveZero = true : "";
                            }

                            for (const mey in glueGroupOfTop[jey]) {
                                const el = glueGroupOfTop[jey][mey];
                                let glueCod = RIGHE.getTag(el, "GLUE");

                                if (topGlueCod === glueCod) {
                                    if (!isGlueHandleCountAboveZero) {
                                        lastGlue = el;
                                        RIGHE.delByUniId(el.MY_UNI_ID);
                                    } else {
                                        //удаляю всех без ручного ввода >0
                                        let glueHandleCount = +RIGHE.getTag(el, "GLUE_COUNT");
                                        if (glueHandleCount <= 0) {
                                            RIGHE.delByUniId(el.MY_UNI_ID);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //вывожу если не было ручного ввода
                if (lastGlue) {
                    let newRiga = RIGHE.createBased(lastGlue);
                    newRiga.COD = "GLUE";
                    newRiga.PZ = glueCount;
                    RIGHE.setTag(newRiga, "GLUE_COUNT", glueCount);
                    RIGHE.addNewToEnd(newRiga);
                    //console.log(`за вырез ${glueCountOfTech}, за плинтус ${glueCountOfPlith}`);
                }
            }
        }
    }
}

//стеновая панель
function stenpan(RIGHE) {
    let panelAllWidth = 0;
    let panelStone;
    let panelCount = 0; //для угловая планка
    let lastPanelDSP;
    let isEarlierAngPlank = false; //отсутствие угловых планок в проекте

    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            const el = RIGHE[key];
            if (el.COD === "STP") {
                el.MY_REAL_L = +RIGHE.getTag(el, 'REAL_L');

                //работа по каменному фартуку
                let panel_product = RIGHE.getTag(el, "PANEL_PRODUCT");
                if ((panel_product === "SUMSUNG" || panel_product === "HIMACS" || panel_product === "GRANDEX")) { //& el.MY_REAL_L
                    panelAllWidth += +el.MY_REAL_L;
                    panelStone = el;
                } else {
                    //считаем панели  
                    if (panel_product == ! "SLOTEX") //SLOTEX выкл 25.06.21
                        panelCount++;

                    lastPanelDSP = el;
                }
            } else if (el.COD === "PLANKA") {
                let angPlankArt = RIGHE.getTag(el, "MYARTICLE");

                //есть нарисованные угловые планки
                if (angPlankArt === "88112" || angPlankArt === "1050"
                    || angPlankArt === "plnakaUgolout_10_600" || angPlankArt === "plnakaUgolin_10_600") {
                    isEarlierAngPlank = true;
                }
            }
        }
    }

    //работа по каменному фартуку
    if (panelAllWidth > 0) {
        let des = "", art = "";

        if (panelAllWidth <= 3600) {
            des = "Изготовление каменного фартука (высота до 700мм) 1 лист (до 3,6пм)";
            art = "workstone4";
        } else if (panelAllWidth <= 4500) {
            des = "Изготовление кам фартука (высота до 700мм) 1,25 листа (от 3,6 до 4,5пм)";
            art = "workstone5";
        } else if (panelAllWidth <= 5400) {
            des = "Изготовление кам фартука (высота до 700мм) 1,5 листа (от 4,5 до 5,4пм)";
            art = "workstone6";
        } else if (panelAllWidth <= 6300) {
            des = "Изготовление кам фартука (высота до 700мм) 1,75 листа (от 5,4 до 6,3пм)";
            art = "workstone7";
        } else if (panelAllWidth <= 7200) {
            des = "Изготовление кам фартука (высота до 700мм) 2 листа (от 6,3 до 7,2пм)";
            art = "workstone23";
        } else if (panelAllWidth <= 8100) {
            des = "Изготовление кам фартука (высота до 700мм) 2,25 листа (от 7,2 до 8,1пм)";
            art = "workstone8";
            //} else if (panelAllWidth < 9000) {
        } else {
            des = "Изготовление кам фартука (высота до 700мм) 2,5 листа (от 8,1 до 9пм)";
            art = "workstone9";
        }

        let riga = RIGHE.create();
        riga.COD = "WORK";
        riga.IDBOX = panelStone.IDBOX; //к последнему
        riga.IDUNICO = panelStone.IDUNICO;
        riga.MODELLO = panelStone.MODELLO;
        riga.QTA = riga.PZ = 1;
        riga.MY_DES = des;
        riga.DETTPREZZO = riga.VAR = `MYNAME=${des};MYARTICLE=${art};WORK_ART=${art}`;
        riga.VARS = RIGHE.setVars(riga.VAR);
        RIGHE.addNewToEnd(riga);
    }

    //выводим угловая планка
    //авторасчет для панелей > 2
    if (isEarlierAngPlank === false && panelCount > 1) {
        //debugger
        let panel_product = RIGHE.getTag(lastPanelDSP, "PANEL_PRODUCT");
        let angPlankCount = panelCount - 1;
        let angPlankArt = "";
        let plankDes = "";
        let isDoubleAngPlank = false; //две планки у SLOTEX
        //let plankP = +lastPanelDSP.P; //глубина

        if (panel_product === "SOYUZ" || panel_product === "KEDR") {
            plankDes = "Угловая планка 600*4";
            angPlankArt = "88112";
        } else if (panel_product === "SLOTEX") {
            //первая планка
            plankDes = "Угловая планка внешн. 600*10";
            angPlankArt = "plnakaUgolin_10_600";
            isDoubleAngPlank = true;
        } else if (!panel_product) {
            //фотопечать кроме ARTE P == 10
            if (lastPanelDSP.P == 4) {
                plankDes = "Угловая планка 600*4";
                angPlankArt = "88112";
            } else if (lastPanelDSP.P == 6) {
                plankDes = "Угловая планка 600*6";
                angPlankArt = "1050";
            }
        }

        if (plankDes) {
            let angPlank = RIGHE.create();
            angPlank.COD = "PLANKA";
            angPlank.IDBOX = lastPanelDSP.IDBOX; //к последнему
            angPlank.IDUNICO = lastPanelDSP.IDUNICO;
            angPlank.MODELLO = lastPanelDSP.MODELLO;
            angPlank.QTA = angPlank.PZ = angPlankCount;
            angPlank.MY_DES = plankDes;
            angPlank.L = lastPanelDSP.L;
            angPlank.A = lastPanelDSP.A;
            angPlank.P = lastPanelDSP.P;
            RIGHE.newTag(angPlank, 'myarticle', angPlankArt);
            RIGHE.newTag(angPlank, 'rod_art', angPlankArt);
            RIGHE.addNewToEnd(angPlank);

            //вторая планка
            if (isDoubleAngPlank) {
                let angPlank2 = RIGHE.create();
                angPlank2.COD = "PLANKA";
                angPlank2.IDBOX = lastPanelDSP.IDBOX; //к последнему
                angPlank2.IDUNICO = lastPanelDSP.IDUNICO;
                angPlank2.MODELLO = lastPanelDSP.MODELLO;
                angPlank2.QTA = angPlank2.PZ = angPlankCount;
                angPlank2.MY_DES = "Угловая планка внутр. 600*10";
                RIGHE.newTag(angPlank2, 'myarticle', "plnakaUgolout_10_600");
                RIGHE.newTag(angPlank2, 'rod_art', "plnakaUgolout_10_600");
                RIGHE.addNewToEnd(angPlank2);
            }
        }
    }
}

//балюстрады
function balustPostProc(RIGHE) {
    //собираю одинаковые карнизы в 1
    let group = RIGHE.group(
        [
            "BALUST",
            "BALUST_PRODUCT",
            "BALUST_COLOR",
            "BALUST_PATINA",
            "BALLUST_SPEC",
            "BALLUST_POV"
        ],
        'BALUSTRADA');
    //console.log(group);

    if (!_.isEmpty(group)) {
        for (const key in group) {
            let length = 0;
            let first = "";

            if (!_.isEmpty(group[key])) {
                for (const jey in group[key]) {
                    const el = group[key][jey];
                    //length += el.MY_REAL_L;
                    length += Math.ceil(+RIGHE.getTag(el, 'REAL_L'));
                    const BALUST_PRODUCT = RIGHE.getTag(el, 'BALUST_PRODUCT');

                    //удаляю кроме первого и кроме BTC
                    if (!(BALUST_PRODUCT === "ВТС" || BALUST_PRODUCT === "Выбор")) {
                        if (first) {
                            RIGHE.delByUniId(el.MY_UNI_ID);
                        } else {
                            first = el.MY_UNI_ID;
                        }
                    }
                    //для Выбор проверяю и  ставлю мин макс, он не объединяется
                    //++написать ф-ю линейной интерполяции
                    if (BALUST_PRODUCT === "Выбор") {
                        let pieces = RIGHE.getTag(el, 'MYSIZES');
                        pieces = pieces.split("*");
                        let min = +pieces[1] || 0;
                        let max = +pieces[2] || 1000;
                        if (+el.L < min) el.L = min;
                        if (+el.L > max) el.L = max;
                    }
                }
                if (first) {
                    let riga = RIGHE.findByUniId(first);
                    let pieces = RIGHE.getTag(riga, 'MYSIZES');
                    //debugger
                    pieces = pieces.split("*");

                    if (pieces[0].toUpperCase() === "F") {
                        //Fixed
                        //only one size
                        let piece = +pieces[1];
                        if (piece < length) {
                            let count = Math.ceil(length / piece);
                            riga.PZ = count;
                        } else {
                            riga.PZ = 1;
                        }

                    } else {
                        //debugger
                        //Strechable
                        let min = +pieces[1] || 0;
                        let max = +pieces[2] || 1000;
                        if (max < length) {
                            let count = Math.floor(length / max);
                            let ostatok = length - count * max;
                            riga.PZ = count;
                            riga.L = max;

                            //add new riga
                            if (ostatok > 0) {
                                let newRiga = RIGHE.createBased(riga);
                                newRiga.MY_UNI_ID = RIGHE.setUniId();
                                newRiga.L = ostatok > min ? ostatok : min;
                                newRiga.PZ = 1;
                                RIGHE.addNewToEnd(newRiga);
                            }
                        } else { //1 позиция
                            riga.PZ = 1;
                            riga.L = length;
                        }
                    }
                }
            }
        }
    }

    //для ВТС 
    //установка мин макс ширины
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            if (RIGHE[key].COD === "BALUSTRADA") {
                let el = RIGHE[key];
                const BALUST_PRODUCT = RIGHE.getTag(el, 'BALUST_PRODUCT');

                if (BALUST_PRODUCT === "ВТС") {
                    const MYSIZES = RIGHE.getTag(el, 'MYSIZES').split("*");
                    const min = +MYSIZES[1]; //200
                    const max = +MYSIZES[2]; //2400

                    if (el.L < min) el.L = min;
                    if (el.L > max) el.L = max;
                }
            }
        }
    }
}

//Свет
function lightPostProc(RIGHE) {
    //инициализация автоподкидки
    //кол-ва драйверов:
    let mod1 = 0;//Удлинитель 935 с 6 выходами 1.5м LL935K.006WT
    let mod2 = 0;//Драйвер 924 30W 10 каналов шнур 2м с вилкой
    let mod3 = 0;//Драйвер 929 20W 4 канала шнур 2м с вилкой
    let mod4 = 0;//Соединитель 933 угловой для свет лент 15см белый арт. LL933K.008WT 
    let mod5 = 0;//Драйвер 924 60W 10 каналов шнур 2м с вилкой арт. LL924L.060WT
    let mod6 = 0;//Соединитель 932 к трансформатору для светх лент 2м белый арт. LL932K.008WT
    let mod7 = 0;//Соединитель 931 прямой для свет белый арт. LL931K.008WT
    let mod8 = 0;//Диффузор 1506 для профиля плоский 2м матовый арт. 17.217.10.026
    let mod9 = 0;//Клипса стальная для профиля 1506 арт 17.411.10.001
    let mod10 = 0;//Прямое соединение для профиля 1506, серебро арт. 17.512.10.012
    let mod11 = 0;//Торцевая заглушка закрытая для профиля 1506, серебристая арт. 17.301.10.012 
    let mod12 = 0;//Торцевая заглушка с отверстием для профиля 1506, серебристая арт. 17.302.10.012
    let mod13 = 0;//Профиль 1506 алюминий для свет ленты 2м  арт.17.101.10.017
    let mod14 = 0;//Светильник 027 ALFA КОМПЛЕКТ с БП диод сенс выкл 600мм бел свет арт.LJ027A.600SET - 1 шт
    let mod15 = 0;//Светильник 027 ALFA диод сенс выкл 600мм бел свет арт.LJ027A.600IS

    let lastLightId = "";
    let pointLight = 0; //кол-во точечных
    let switchCount = 0;//кол-во выключателей
    let lightClipsCount = 0; //кол-во ветильник_клипса 215 на стек полку L66мм
    let alfaLight = 0;

    //объединение ленты
    let group = RIGHE.group('LIGHT_ART', "LIGHT");
    //console.log(group);
    if (!_.isEmpty(group)) {
        for (const key in group) {
            let length = 0; //общая длина
            let first = ""; //ID

            if (!_.isEmpty(group[key])) {
                for (const jey in group[key]) {
                    const el = group[key][jey];
                    let LIGHT_TYPE = RIGHE.getTag(el, "LIGHT_TYPE");

                    if (LIGHT_TYPE === "lenta" || LIGHT_TYPE === "lenta2") {
                        length += el.L;

                        //кроме первого
                        if (first) {
                            RIGHE.delByUniId(el.MY_UNI_ID);
                        } else {
                            first = el.MY_UNI_ID;
                        }
                    } else if (LIGHT_TYPE === "alfa") {
                        ++alfaLight;
                    }
                }

                if (length) {
                    let riga = RIGHE.findByUniId(first);
                    let LIGHT_TYPE = RIGHE.getTag(riga, "LIGHT_TYPE");

                    if (LIGHT_TYPE === "lenta" || LIGHT_TYPE === "lenta2") {
                        riga.MY_REAL_L = length;
                        riga.L = 5000;
                        if (length <= 5000) {
                            riga.PZ = 1;
                        } else {
                            riga.PZ = Math.ceil(length / 5000);
                        }
                    }
                }
            }
        }
    }

    //подсчет выключателей
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key) && RIGHE[key].COD === "SWITCH") {
            const el = RIGHE[key];
            switchCount += el.PZ;
        }
    }

    //реализация (сбор) автоподкидки
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key) && RIGHE[key].COD === "LIGHT") {
            const el = RIGHE[key];
            lastLightId = el.MY_UNI_ID;
            //автоподкидка 1шт
            let mods = RIGHE.getTag(el, "LIGHT_MODS").split('*');
            let LIGHT_TYPE = RIGHE.getTag(el, "LIGHT_TYPE");
            let LIGHT_ART = RIGHE.getTag(el, "LIGHT_ART");

            if (mods) {
                for (let i = 0; i < mods.length; i++) {
                    if (+mods[i] === 1) mod1 = 1;
                    if (+mods[i] === 2) mod2 = 1
                    if (+mods[i] === 3) mod3 = 1
                    if (+mods[i] === 4) mod4 = 1
                }
            }

            //логика для кол-ва автоподкидки.
            let logic = RIGHE.getTag(el, "LIGHT_LOGIC");
            //точечный
            if (logic === "2*3*1") {
                pointLight++;

                //обычная лента
            } else if (logic === "5*2*1*6*4") {
                if (LIGHT_TYPE === "lenta") {
                    //лента только одна осталась
                    //mod5
                    if (LIGHT_ART === "KZ004202" || LIGHT_ART === "KZ004203" || LIGHT_ART === "KZ004204") {
                        if (el.MY_REAL_L >= 3000 & el.MY_REAL_L <= 5000) {
                            mod5++;
                        } else if (el.MY_REAL_L > 5000) {
                            mod5 += el.PZ;
                        }
                    }

                    //mod2
                    if ((LIGHT_ART === "KZ004202" || LIGHT_ART === "KZ004203" || LIGHT_ART === "KZ004204")
                        & el.MY_REAL_L < 3000) mod2++;
                    if (LIGHT_ART === "LE250A.060WS") mod2 += el.PZ;

                    //mod7
                    mod7 += el.PZ;

                    //mod4
                    mod4 += el.PZ;

                    //mod1
                    mod1 += switchCount;

                    //mod6
                    mod6 += switchCount;
                }

            } else if (logic === "5*2*1*6*4*8*9*10*11*12*13") { //не обычная лента
                if (LIGHT_TYPE === "lenta2") {
                    //лента только одна позиция осталась
                    // в MY_REAL_L вся сумма
                    //здесь так же как и у обычная лента
                    //mod5
                    if (LIGHT_ART === "KZ004202" || LIGHT_ART === "KZ004203" || LIGHT_ART === "KZ004204") {
                        if (el.MY_REAL_L >= 3000 & el.MY_REAL_L <= 5000) {
                            mod5++;
                        } else if (el.MY_REAL_L > 5000) {
                            mod5 += el.PZ;
                        }
                    }

                    //mod2
                    if ((LIGHT_ART === "KZ004202" || LIGHT_ART === "KZ004203" || LIGHT_ART === "KZ004204")
                        & el.MY_REAL_L < 3000) mod2++;
                    if (LIGHT_ART === "LE250A.060WS") mod2 += el.PZ;

                    //mod7
                    mod7 += el.PZ;

                    //mod4
                    mod4 += el.PZ;

                    //mod1
                    mod1 += switchCount;

                    //mod6
                    mod6 += switchCount;

                    //дополнительно
                    //mod13
                    mod13 += Math.ceil(el.MY_REAL_L / 2000);

                    //mod8
                    mod8 += Math.ceil(el.MY_REAL_L / 2000);

                    //mod9
                    mod9 += Math.ceil(el.MY_REAL_L / 1000) * 2;

                    //mod10
                    //mod10 += Math.ceil(el.MY_REAL_L / 4000); //20,08,21 выкл

                    //mod11
                    mod11 += Math.ceil(el.MY_REAL_L / 2000) * 1;

                    //mod12
                    mod12 += Math.ceil(el.MY_REAL_L / 2000) * 1;
                }
            } else if (logic === "1") { //Светильник_клипса 215 на стек полку L66мм
                lightClipsCount++;
            }
        }
    }

    //точечные
    if (pointLight > 0 && pointLight <= 10) {
        mod3++;
    } else if (pointLight > 10) {
        mod2++;
    }
    if (pointLight > 0 && pointLight <= 6) {
        mod1++;
    } else if (pointLight > 6 && pointLight < 11) {
        mod1 += 2;
    } else if (pointLight >= 11) {
        mod1 += 3;
    }

    //Светильник_клипса
    if (lightClipsCount) {
        if (lightClipsCount > 0 & lightClipsCount <= 6) {
            mod1 += 1;
        } else if (lightClipsCount >= 7 & lightClipsCount <= 11) {
            mod1 += 2;
        } else if (lightClipsCount >= 12 & lightClipsCount <= 17) {
            mod1 += 3;
        } else if ((lightClipsCount >= 18 & lightClipsCount <= 23) | lightClipsCount > 23) {
            mod1 += 4;
        }
    }

    //alfa
    if (alfaLight > 0) {
        switch (alfaLight) {
            case 1:
                ++mod14;
                break;
            case 2:
                ++mod15; //LJ027A.600IS
                ++mod1;  //LL935K.006WT
                ++mod14; //LJ027A.600SET
                break;
            case 3:
                mod15 += 3;
                ++mod1;
                ++mod3; //LL929L.020WT
                break;
            case 4:
                mod15 += 4;
                ++mod2; //LL924L.030WT
                mod1 += 2;
                break;
            case 5:
                mod15 += 5;
                ++mod5; //LL924L.060WT
                mod1 += 2;
                break;
            case 6:
                mod15 += 6;
                ++mod5;
                mod1 += 3;
                break;
            case 7:
                mod15 += 7;
                ++mod5;
                mod1 += 3;
                break;
            case 8:
                mod15 += 8;
                ++mod5;
                mod1 += 4;
                break;
            case 9:
                mod15 += 9;
                ++mod5;
                mod1 += 4;
                break;
            default:
                mod15 += 10;
                ++mod5;
                mod1 += 5;
                break;
        }
    }

    /*console.log("mod1:" + mod1, "mod2:" + mod2, "mod3:" + mod3, "mod4:" + mod4, "mod5:" + mod5
        , "mod6:" + mod6, "mod7:" + mod7, "mod8:" + mod8, "mod9:" + mod9, "mod10:" + mod10,
        "mod11:" + mod11, "mod12:" + mod12, "mod13:" + mod13);*/

    //вывод автоподкидки
    if (lastLightId) {
        let rigaLight = RIGHE.findByUniId(lastLightId);

        //Удлинитель 935 с 6 выходами 1.5м
        if (mod1 > 0) {
            addRIga("LIGHT", "Удлинитель 935 с 6 выходами 1.5м", mod1, rigaLight, "LL935K.006WT");
            mod1 = 0;
        }
        if (mod2 > 0) {
            addRIga("LIGHT", "Драйвер 924 30W, 10 каналов шнур 2м с вилкой", mod2, rigaLight, "LL924L.030WT");
            mod2 = 0;
        }
        if (mod3 > 0) {
            addRIga("LIGHT", "Драйвер 929 20W 4 канала шнур 2м с вилкой", mod3, rigaLight, "LL929L.020WT");
            mod3 = 0;
        }
        if (mod4 > 0) {
            addRIga("LIGHT", "Соединитель 933 угловой для свет лент 15см белый", mod4, rigaLight, "LL933K.008WT");
            mod4 = 0;
        }
        if (mod5 > 0) {
            addRIga("LIGHT", "Драйвер 924 60W 10 каналов шнур 2м с вилкой", mod5, rigaLight, "LL924L.060WT");
            mod5 = 0;
        }
        if (mod6 > 0) {
            addRIga("LIGHT", "Соединитель 932 к трансформатору для светх лент 2м белый", mod6, rigaLight, "LL932K.008WT");
            mod6 = 0;
        }
        if (mod7 > 0) {
            addRIga("LIGHT", "Соединитель 931 прямой для свет белый", mod7, rigaLight, "LL931K.008WT");
            mod7 = 0;
        }
        if (mod8 > 0) {
            addRIga("LIGHT", "Диффузор 1506 для профиля плоский 2м матовый", mod8, rigaLight, "17.217.10.026");
            mod8 = 0;
        }
        if (mod9 > 0) {
            addRIga("LIGHT", "Клипса стальная для профиля 1506", mod9, rigaLight, "17.411.10.001");
            mod9 = 0;
        }
        if (mod10 > 0) {
            addRIga("LIGHT", "Прямое соединение для профиля 1506, серебро", mod10, rigaLight, "17.512.10.012");
            mod10 = 0;
        }
        if (mod11 > 0) {
            addRIga("LIGHT", "Торцевая заглушка закрытая для профиля 1506, белая", mod11, rigaLight, "17.301.10.012");
            mod11 = 0;
        }
        if (mod12 > 0) {
            addRIga("LIGHT", "Торцевая заглушка с отверстием для профиля 1506, белая", mod12, rigaLight, "17.302.10.012");
            mod12 = 0;
        }
        if (mod13 > 0) {
            addRIga("LIGHT", "Профиль 1506 алюминий для свет ленты 2м", mod13, rigaLight, "17.101.10.017");
            mod13 = 0;
        }
        if (mod14 > 0) {
            addRIga("LIGHT", "Светильник 027 ALFA КОМПЛЕКТ с БП диод сенс выкл 600мм бел. свет", mod14, rigaLight, "LJ027A.600SET");
            mod14 = 0;
        }
        if (mod15 > 0) {
            addRIga("LIGHT", "Светильник 027 ALFA диод сенс выкл 600мм бел. свет", mod15, rigaLight, "LJ027A.600IS");
            mod15 = 0;
        }
    }

    //удаляю ALFA
    for (const key in RIGHE) {
        if (Object.hasOwnProperty.call(RIGHE, key)) {
            const el = RIGHE[key];
            let LIGHT_TYPE = RIGHE.getTag(el, "LIGHT_TYPE");
            if (LIGHT_TYPE === "alfa") {
                RIGHE.delByUniId([el.MY_UNI_ID]);
            }
        }
    }

    function addRIga(cod, des, pz, rigaLight, article) {
        let riga = RIGHE.create();
        riga.COD = cod;
        riga.MY_DES = des;
        riga.QTA = riga.PZ = pz;
        riga.IDBOX = rigaLight.IDBOX;
        riga.IDUNICO = rigaLight.IDUNICO;
        riga.ORDINAMENTO = rigaLight.ORDINAMENTO;
        RIGHE.newTag(riga, "MYARTICLE", article);
        RIGHE.newTag(riga, "LIGHT_ART", article);
        RIGHE.addNewToEnd(riga);
    }
}

//опора
function tubePostProc(RIGHE) {
    let group = RIGHE.group('TUBE_ART', "TUBE");

    //console.log(group);
    if (!_.isEmpty(group)) {
        for (const key in group) {
            let count = 0;
            let first = ""; //ID

            if (!_.isEmpty(group[key])) {
                for (const jey in group[key]) {
                    const el = group[key][jey];
                    const article = RIGHE.getTag(el, 'TUBE_ART');

                    //Опора для бара наклонная 
                    if (article === "19786") {
                        count++;

                        //кроме первого
                        if (first) {
                            RIGHE.delByUniId(el.MY_UNI_ID);
                        } else {
                            first = el.MY_UNI_ID;
                        }
                    }
                }

                if (count) {
                    let riga = RIGHE.findByUniId(first);
                    if (!even(count)) count++;
                    riga.PZ = count / 2;
                }
            }
        }
    }
}

//различная автоподкидка
//артикул у ручек
//бином эмаль
//работа workroad4
function difItem(RIGHE) {

    //брус
    //подсчет
    let korpCount = 0;
    let firstKorp;
    let idList = ["SH8072", "SH9072", "SH8092", "SH9092",
        "T8072IT", "T9072IT", "T8072", "T9072", "TUG9072"];
    for (let key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            let el = RIGHE[key];
            let KORPUSID = RIGHE.getTag(el, "KORPUSID");
            if (KORPUSID && idList.includes(KORPUSID)) {
                //let NESTKORP = RIGHE.getTag(el, "NESTKORP");
                korpCount++;
                if (!firstKorp) firstKorp = el;
                // if (NESTKORP !== "2") {
                //     korpCount++;
                //     //console.log(KORPUSID);
                //     if (!firstKorp) firstKorp = el; //подкидываю в первый корпус
                // }
            }
        }
    }
    //подкидыш бруса
    //let krep = RIGHE.findByCod('KREP');
    if (korpCount && firstKorp) {
        //console.log(korpCount);
        let count = Math.ceil(korpCount / 2) + 1; //на каждые 2 карпуса
        let brus = RIGHE.create();
        brus.COD = "BRUS";
        brus.L = 2000;
        brus.PZ = count;
        brus.IDBOX = firstKorp.IDBOX;
        brus.ORDINAMENTO = firstKorp.ORDINAMENTO;
        brus.MY_DES = 'Соединительный брус для ДВП белый 2м';
        RIGHE.newTag(brus, 'myarticle', 'brusDVP2m');
        RIGHE.newTag(brus, 'brus_art', 'brusDVP2m');
        RIGHE.addNewToEnd(brus);
    }

    //К корпусам
    let idKorpus = "";
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            if (RIGHE.getTag(RIGHE[key], 'KORPUSNAME')) {
                idKorpus = RIGHE[key].IDBOX;
                break;
            }
        }
    }
    if (idKorpus > 0) {
        //Демпфер самоклеющийся
        let dempf = RIGHE.create();
        dempf.COD = "DAMPER";
        dempf.PZ = 1;
        dempf.IDBOX = idKorpus;
        dempf.MY_DES = 'Демпфер самоклеющийся 9х2мм лист-50шт';
        RIGHE.newTag(dempf, 'myarticle', '22007');
        RIGHE.newTag(dempf, 'demper_art', '22007');
        RIGHE.addNewToEnd(dempf);

        //Заглушка самоклеящаяся 14мм
        let dempf2 = RIGHE.create();
        dempf2.COD = "DAMPER";
        dempf2.PZ = 1;
        dempf2.IDBOX = idKorpus;
        dempf2.MY_DES = 'Заглушка самоклеящаяся 14мм белая BEYAZ лист-50шт';
        RIGHE.newTag(dempf2, 'myarticle', '21037');
        RIGHE.newTag(dempf2, 'demper2_art', '21037');
        RIGHE.addNewToEnd(dempf2);
    }

    //Комплект винтов с шайбами для ручек
    let isVint = "";
    let vintCount = 0;
    let isVintFasad = false;
    let isVintHandle = false;
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            const el = RIGHE[key];
            const FREZ_COD = RIGHE.getTag(el, "FREZ_COD");
            const FREZ_COD_LIST = [
                "handpul_V",
                "handpul_PV",
                "handpul_HV",
                "handpul_HN",
                "modern_22_handle_vibor",
                "undina",
                "nimfa",
                "verlioka",
                "rodeo",
                "AL"
            ];

            if (el.COD === "FASAD" && el.P >= 19 && !FREZ_COD_LIST.includes(FREZ_COD)) {
                isVint = el.IDBOX;
                isVintFasad = true;
            }
            if (el.COD === "HANDLE") {
                const GROUP = RIGHE.getTag(el, "GROUP");
                if (GROUP !== "nkld") isVintHandle = true;
            }
            if (isVintFasad && isVintHandle) break;
        }
    }

    if (isVintFasad && isVintHandle) vintCount++;

    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            const el = RIGHE[key];
            if (el.COD === "HANDLE") {
                //атрикула ручек
                let arr = [
                    '106.62.604',
                    '106.62.605',
                    '106.62.606',
                    '106.61.943',
                    '106.61.944',
                    'RS530MAB.1\\128',
                    'RS530AC.1\\128',
                    'RS530AP.1\\128',
                    'RC501MAB.1',
                    'RC501AC.1',
                    'RC501AP.1',
                    'RS531MAB.1\\128',
                    'RS531AP.1\\128',
                    'RS195MBSN.1\\160',
                ]
                if (arr.includes(RIGHE.getTag(el, 'MYARTICLE'))) {
                    if (!isVint) isVint = el.IDBOX;
                    vintCount++;
                    break;
                }
            }
        }
    }
    if (isVint && vintCount > 0) {
        let vint = RIGHE.create();
        vint.COD = "VINT";
        vint.PZ = vintCount;
        vint.IDBOX = isVint;
        vint.MY_DES = 'Комплект винтов с шайбами для ручек М4х25 40шт для фасадов 19мм и более';
        RIGHE.newTag(vint, 'myarticle', '182546');
        RIGHE.newTag(vint, 'vint_art', '182546');
        RIGHE.addNewToEnd(vint);
    }

    //артикул у ручек
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key)) {
            if (RIGHE[key].COD === "HANDLE") {
                let art = RIGHE.getTag(RIGHE[key], 'HANDLE_ARTIKUL_FIERA');
                if (art) {
                    let codingValue = [
                        ['/', '0x2F'],
                        [',', '0x2C']
                    ]

                    //раскодировщик
                    function encoding(str) {
                        for (let i = 0; i < codingValue.length; i++) {
                            const el = codingValue[i];
                            if (str.indexOf(el[1] != -1)) {
                                str = str.split(el[1]).join(el[0]);
                            }
                        }
                        return str
                    }

                    let newArt = encoding(art);
                    RIGHE.setTag(RIGHE[key], 'HANDLE_ARTIKUL_FIERA', newArt);
                    RIGHE.setTag(RIGHE[key], 'MYARTICLE', newArt);
                }
            }
        }
    }

    //Бином эмаль с интегр ручкой
    /*let isIntegrHandle = false;
    for (const key in RIGHE) {
        if (RIGHE.hasOwnProperty(key) && RIGHE[key].COD === "FASAD") {
            let el = RIGHE[key];
        }
    }*/

    //Работа workroad4 всегда
    let work4 = RIGHE.create();
    work4.COD = "WORK";
    work4.IDBOX = work4.PZ = 1;
    work4.MY_DES = 'Может понадобиться Замеры помещения в черте города';
    RIGHE.newTag(work4, 'myarticle', 'workroad4');
    RIGHE.newTag(work4, 'work_art', 'workroad4');
    RIGHE.addNewToEnd(work4);
}

function topPOut(P_In, riga, RIGHE) {
    let topManuf = RIGHE.getTag(riga, "TOP_MANUFACTURER");
    let topFormCod = RIGHE.getTag(riga, "TOP_FORM_COD");
    let topForm = RIGHE.getTag(riga, "RADIUS");
    let topKromka = RIGHE.getTag(riga, "KROMKA");
    let top_find_p = RIGHE.getTag(riga, "TOP_FIND_P"); //из проги беру размер
    let P_Out = P_In;

    //if (topManuf === "KEDR" || topManuf === "SLOTEX" || topManuf === "SOYUZ") {
    P_Out = top_find_p;
    //}

    return P_Out;
}

//вывод L_Out длин для 1с для столешек
export function topLOut(L_In, riga, RIGHE) {
    //debugger;
    let topManuf = RIGHE.getTag(riga, "TOP_MANUFACTURER").toUpperCase();
    let topFormCod = RIGHE.getTag(riga, "TOP_FORM_COD");
    let topForm = RIGHE.getTag(riga, "RADIUS");
    let topKromka = RIGHE.getTag(riga, "KROMKA");
    if (topKromka === "ABS складская") topKromka = "ABS"; //fix 26.04.2021
    let top_find_l = +RIGHE.getTag(riga, "TOP_FIND_L"); //из проги беру размер
    const KROMKA_COD = RIGHE.getTag(riga, "KROMKA_COD");
    let L_Out = 0;
    if (topFormCod === "01" || topForm === "Прямоуголная") {
        if (topManuf === "SOYUZ") {
            if (topKromka === "ABS" && KROMKA_COD != "33") { //без абс склад
                L_Out = L_In;
            } else {
                if (L_In == 3000) {
                    L_Out = 3050;
                } else if (L_In == 4150) {
                    L_Out = 4200;
                } else {
                    L_Out = L_In;
                }
            }
        } else if (topManuf === "KEDR") {
            if (L_In == 3000) {
                L_Out = 3050;
            } else if (L_In === 4000) {
                L_Out = 4100;
            } else {
                L_Out = L_In;
            }
        } else if (topManuf === "SLOTEX" || topManuf === "SLOTEXKL") {
            if (topKromka === "ABS") {
                L_Out = L_In;
            } else {
                if (L_In == 3000) {
                    L_Out = 3050;
                } else if (L_In == 4150) {
                    L_Out = 4200;
                } else {
                    L_Out = L_In;
                }
            }
        } else if (topManuf === "SUMSUNG" || topManuf === "HIMACS" || topManuf === "GRANDEX" || topManuf === "STONECS") {
            if (L_In == 3500) {
                L_Out = 3680;
            } else if (L_In == 2625) {
                L_Out = 2760;
            } else if (L_In == 1750) {
                L_Out = 1840;
            } else if (L_In == 875) {
                L_Out = 920;
            } else {
                L_Out = L_In;
            }
        } else if (topManuf === "ANTARES") {
            if (topKromka === "ABS") {
                L_Out = L_In;
            } else {
                if (L_In == 3000) {
                    L_Out = 3050;
                } else if (L_In == 4050) {
                    L_Out = 4200;
                } else if (L_In == 4150) {
                    L_Out = 4200;
                } else {
                    L_Out = L_In;
                }
            }
        } else {
            L_Out = L_In;
        }

    } else { //фигурные
        if (topManuf === "SLOTEXKL") { //topManuf === "SLOTEX" || 
            if (L_In == 3000) {
                L_Out = 3050;
            } else if (L_In == 4150) {
                L_Out = 4200;
            } else {
                L_Out = L_In;
            }
        } else if (topManuf === "SLOTEX") { //topManuf === "SLOTEX" || 
            if (topKromka === "ABS") {
                L_Out = L_In;
            } else {
                if (L_In == 3000) {
                    L_Out = 3050;
                } else if (L_In == 4150) {
                    L_Out = 4200;
                } else {
                    L_Out = L_In;
                }
            }
        } else if (topManuf === "SUMSUNG" || topManuf === "HIMACS" || topManuf === "GRANDEX") {
            if (L_In == 3500) {
                L_Out = 3680;
            } else if (L_In == 2625) {
                L_Out = 2760;
            } else if (L_In == 1750) {
                L_Out = 1840;
            } else if (L_In == 875) {
                L_Out = 920;
            }
        } else if (topManuf === "SOYUZ") {
            if (topKromka === "ABS" && KROMKA_COD != "33") {
                L_Out = L_In;
            } else {
                if (L_In == 3000) {
                    L_Out = 3050;
                } else if (L_In == 4150) {
                    L_Out = 4200;
                } else {
                    L_Out = L_In;
                }
            }
        } else if (topManuf === "KEDR") {
            if (L_In == 3000) {
                L_Out = 3050;
            } else if (L_In === 4000) {
                L_Out = 4100;
            } else {
                L_Out = L_In;
            }
        } else if (topManuf === "ANTARES") {
            if (topKromka === "ABS") {
                L_Out = L_In;
            } else {
                if (L_In == 3000) {
                    L_Out = 3050;
                } else if (L_In == 4050) {
                    L_Out = 4200;
                } else {
                    L_Out = L_In;
                }
            }
        } else {
            L_Out = top_find_l; //спорный момент
        }
    }
    //console.log(topManuf, topForm, topKromka, top_find_l, KROMKA_COD, L_In, L_Out);
    return L_Out;
}