﻿var x2js = new X2JS();

var tableTr = [];
var tempText = "";
var trStyle;
let pageInfo = [];
var jsonPrev = "";
jsonPrev = (x2js.xml_str2json(XML_FILE));
//Object.setPrototypeOf(jsonPrev, testObj);
//jsonPrev.PREV.RIGHE.__proto__ = new classRiga();
//console.log(x2js);

//console.log(jsonPrev);

window.onload = function () {
	main(); //
}

function main() {
	//console.log((x2js.xml_str2json(XML_FILE)));
	//Исправляю var
	console.log(jsonPrev);
	var indexRiga = 0;
	delete jsonPrev.PREV.TESTA.VAR.__text;
	//console.log(jsonPrev.PREV.RIGHE.RIGA3.L);

	let forI = 1;
	for (let key in jsonPrev.PREV.RIGHE) {
		let item = jsonPrev.PREV.RIGHE[key];
		//уникальный номер
		item.MY_UNI_ID = item.COD + "_" + forI;

		item.IDBOX = Number(item.IDBOX);
		item.A = Number(item.A);
		item.L = Number(item.L);
		item.P = Number(item.P);
		item.ELMC = Number(item.ELMC);
		item.IDUNICO = Number(item.IDUNICO);
		item.QTA = Number(item.QTA);
		item.PZ = Number(item.PZ);
		item.ORDINAMENTO = Number(item.ORDINAMENTO);

		forI++;
		//процедура установки id столешки из тэга
		if (item.COD === "TOP") {
			item.IDBOX = Number(+findTag(item.VAR, "TOPID")) + 1;
		}
	}

	//Преобразую длинномеры
	getDlin(jsonPrev.PREV, 'PLINTUS', "PLINT_COUNT", "");
	getDlin(jsonPrev.PREV, 'SOCLE', "SOCLE_COUNT_MIN", "SOCLE_COUNT_MAX");
	getDlin(jsonPrev.PREV, 'KARNIZ', "KARNIZ_COUNT", "");
	getDlin(jsonPrev.PREV, 'KARNIZN', "KARNIZN_COUNT", "");
	getDlin(jsonPrev.PREV, 'BALUSTRADA', "BALUSTRADA_COUNT", "");
	//console.log(jsonPrev.PREV.RIGHE.RIGA4);

	//Выбираем только столешницы по полю COD = TOP
	var top = _.filter(jsonPrev.PREV.RIGHE, { 'COD': 'TOP' });

	//Перебираем массив столешниц
	top = top.map(function (item) {
		//Преобразуем сторку вариантов в массив
		item.VarArr = stringToMap(item.VAR);

		//Дописываю свойство - реальный размер
		item.REAL_L = +item.L;
		//console.log(item.REAL_L);
		return item;
	});
	//console.log(top);

	//Группируем столешницы по вариантам
	const keyVarGroup = ['TOP_MANUFACTURER', 'STOLESH_MAT', 'TOLSHINA', 'KROMKA'];
	var topGroup = _.groupBy(top, function (item) {
		//подбираем варианты для объединения столшниц
		// 1. TOP_MANUFACTURER - производитель 
		// 2. STOLESH_MAT - матариал 
		// 3. TOLSHINA - высота
		// 4. KROMKA - кромка по лицу

		//Фильтруем только нужные варианты
		var GroupKey = item.VarArr.filter(function (iVar) {
			return _.indexOf(keyVarGroup, iVar[0]) >= 0
		});
		//Преобразуем в строку <вариант>=<Значение>
		GroupKey = GroupKey.map(function (ikey) {
			return _.join(ikey, '=');
		});
		//объединяем все варинты в троку с разделителем ";"
		return _.join(GroupKey, ';');
	});

	//console.log(topGroup);
	typeAlgoritm = 1;
	if (typeAlgoritm === 1) {
		//Перебираем группы для группировки
		_.forEach(topGroup, function (itemGrp) {
			//Пребираем саму группу 
			//получаем длины хлыстов
			var fulltop = [];
			_.forEach(itemGrp, function (item) {
				var top_l = item.VarArr.filter(function (o) {
					return (o[0] === 'TOP_L')
				});
				fulltop = _.union(fulltop, top_l[0][1].split('*').map(function (item) { return Number(item) }));
				item.L = upInt(fulltop, item.L); //к столешнице присваиваю L
				//item.VAR
				//console.log(item);
			});
		});
		//console.log(topGroup);


	} else if (typeAlgoritm === 2) { //не исп
		//Перебираем группы для группировки
		for (let key in topGroup) {
			itemGrp = topGroup[key];
			//Пребираем саму группу 
			var fulltop = itemGrp.reduce(function (summ, item) {
				return summ + Number(item.L)
			}, 0);
			//console.log(fulltop);
			var arrTop_l = [];
			_.forEach(itemGrp, function (item) {
				var top_l = item.VarArr.filter(function (o) {
					return (o[0] === 'TOP_L')
				});
				arrTop_l = _.union(fulltop, top_l[0][1].split('*').map(function (item) { return Number(item) }));
			});

			var res = [];

			arrTop_l.sort(function (a, b) { return b - a; });
			for (let i = 0; i < arrTop_l.length; i++) {
				var cnt = Math.trunc(fulltop / arrTop_l[i]);
				res.push([arrTop_l[i], cnt, key]);
				fulltop = arrTop_l[i] * cnt;
			}
			if (fulltop > 0) {
				res[res.length - 1][1] = res[res.length - 1][1] + 1;
			}
		};
		//console.log(res);
	}

	//prevReNumber();
	//Переписываем XML 
	//Удаляем столешницы 
	let arrK = [];

	//перемещаю столешки вверх к своим внутренностям
	//for (let i = 0; i < arrK.length; i++) {
	let go = new Boolean(false);
	for (let key in jsonPrev.PREV.RIGHE) {
		let item = jsonPrev.PREV.RIGHE[key];
		//console.log(key + ', ' + item.COD);

		if (jsonPrev.PREV.RIGHE[key].COD === "TOP") {
			let index = _.parseInt(key.replace('RIGA', ''));
			let topIdBox = +jsonPrev.PREV.RIGHE[key].IDBOX;
			go = true;
			//console.log('Начало второго цикла TOP для ' + key);

			for (let meow in jsonPrev.PREV.RIGHE) {
				//console.log("meow.IDBOX:" + jsonPrev.PREV.RIGHE[meow].IDBOX + "=="
				//	+ "key.IDBOX:" + topIdBox + "  &&&&  " + "go:" + go);
				if (jsonPrev.PREV.RIGHE[meow].IDBOX == topIdBox &&
					jsonPrev.PREV.RIGHE[meow].COD !== "TOP" && go) {
					let innerIndex = _.parseInt(meow.replace('RIGA', ''));
					//console.log('Результат второго цикла: найденный в условии ' + meow);
					//console.log(key + ', ' + innerIndex + ', ' + index);
					go = false; //выполнять только к 1му элементу
					arrK.push(innerIndex);
				}
			}
		}
	}
	//}
	//console.log(arrK);

	//console.log(indexRiga + 1);
	for (let key in jsonPrev.PREV.RIGHE) {
		indexRiga = _.parseInt(key.replace('RIGA', '')); //
		if (jsonPrev.PREV.RIGHE[key].COD === "TOP") {
			delete jsonPrev.PREV.RIGHE[key];
		}
	}

	//Кол-во строк в righe
	//console.log(jsonPrev.PREV.RIGHE.RIGA4);
	//indexRiga = Object.keys(jsonPrev.PREV.RIGHE).length + 1; //
	indexRiga++;
	//Добавляем новые строчки //тут могут быть косяки
	let count = 0;
	_.forEach(topGroup, function (itemGrp) {
		_.forEach(itemGrp, function (item) {
			delete item['VarArr'];
			//if (!jsonPrev.PREV.RIGHE['riga' + indexRiga]) {
			jsonPrev.PREV.RIGHE['RIGA' + indexRiga] = item;
			//addNewRiga(item, arrK[count]);
			//addNewRiga(item, arrK[count]);
			//addNewRiga2(item, arrK[count]);
			//console.log("arrK[" + count + "]=" + arrK[count]);
			indexRiga++;
			count++;
			arrK[count] += count;
			//}
		});
	});

	//Дописываю в описание длину + ЛЕНТА КРОМКА 
	for (let key in jsonPrev.PREV.RIGHE) {
		let nn = jsonPrev.PREV.RIGHE[key];
		if (nn.COD === "TOP") {
			//console.log(nn.var + ' ' + nn.l)
			let kk = findTag(nn.VAR, "MYNAME");
			let topID = Number(_.parseInt(key.replace('RIGA', ''))) + 1;

			//вытягиваю из внутренностей ORDINAMENTO
			for (let inKey in jsonPrev.PREV.RIGHE) {
				let item = jsonPrev.PREV.RIGHE[inKey];
				if (item.COD !== "TOP" && item.IDBOX == nn.IDBOX) {
					//console.log(item.ORDINAMENTO);
					nn.ORDINAMENTO = item.ORDINAMENTO;
				}
			}

			//зная уже длину стола хлыста, запоминаю её фактический размер из проекта для кромки
			let top_l = nn.L;
			//console.log(topID);

			//ищю длину для вывода
			//nn.CALC_L = nn.L; //расчетная длина
			nn.L = topLOut(nn.L, nn.VAR); //перезаписываю длину для 1с
			nn.A = topPOut(nn.A, nn.VAR);
			nn.VAR = findTagReplace(nn.VAR, "MYNAME", kk + nn.L + ". Ш" + nn.A);
			//переназначаю индекс на номер последний элемент
			for (let key in jsonPrev.PREV.RIGHE) { indexRiga++ }

			//ЛЕНТА КРОМКА добавляю строчку
			//Объединяется декор, производитель, высота
			let forLentaWidth = 0; //сумма ленты
			let lentaCount = 1; //количество ленты
			let lentaHeight = 45; //высота ленты
			let lentaLength = 3050; //длина ленты
			let manufacturer = findTag(nn.DETTPREZZO, "TOP_MANUFACTURER");
			let stolesh_mat = findTag(nn.DETTPREZZO, "STOLESH_MAT");

			//собираю длину
			if (findTag(nn.VAR, "KROMKA_L_COD") === "12") forLentaWidth = +nn.A;
			if (findTag(nn.VAR, "KROMKA_R_COD") === "12") forLentaWidth = +nn.A + forLentaWidth;
			if (findTag(nn.VAR, "KROMKA_Z_COD") === "12") forLentaWidth = top_l + forLentaWidth;
			//console.log(forLentaWidth);
			//console.log(findTag(nn.DETTPREZZO, "KROMKA_L_COD"));

			if (forLentaWidth > 0) {
				//считаю
				if (manufacturer === "KEDR" || manufacturer === "SOYUZ") {
					if (manufacturer === "KEDR") lentaHeight = 44;
					if (forLentaWidth > 0) lentaCount = Math.ceil(forLentaWidth / 3000); //округляю в большую
				} else if (manufacturer === "SLOTEX") {
					if (forLentaWidth <= 3000) { } //стандартно
					else if (forLentaWidth <= 4150) lentaLength = 4200;
					else if (forLentaWidth > 4150) lentaCount = Math.ceil(forLentaWidth / 4150);
				}
				//записываю
				let newRiga = CreateRiga();
				newRiga.COD = "KROMKA";
				newRiga.L = lentaLength;
				newRiga.A = lentaHeight;
				newRiga.P = "0.6";
				newRiga.PZ = lentaCount; //количечтво
				newRiga.VAR = "MYNAME=Лента кромка с клеем " + manufacturer + ", " + stolesh_mat + ", Д" + lentaLength + ", Ш" + newRiga.A + ";KROMKA_MAT=" + stolesh_mat + ";KROMKA_MANUFAKTURER=" + manufacturer;
				newRiga.IDBOX = nn.IDBOX;
				newRiga.IDUNICO = nn.IDUNICO;
				newRiga.ORDINAMENTO = nn.ORDINAMENTO;
				newRiga.MY_UNI_ID = newRiga.COD + '_' + indexRiga;
				jsonPrev.PREV.RIGHE['RIGA' + indexRiga + 1] = newRiga;//ЛЕНТА КРОМКА //тут могут быть косяки из-за indexRiga
				//addNewRiga(newRiga, topID);
				//addNewRiga2(newRiga, topID);
			}
		}
	}

	//console.log(jsonPrev);
	//getModulDlin(); //рисую модуль по длинномерам устарело не исп
	//drawModuleDlin(); //new!!!
	//topPostProc();
	drawImgPreview($(".addprint"), IMG_LIST.split(';'));

	for (let key in jsonPrev.PREV.RIGHE) {
		let service = findTag(jsonPrev.PREV.RIGHE[key].VAR, "SERVICE");
		if (service === "delete") {
			delete jsonPrev.PREV.RIGHE[key];
		}
	}




	myTable(); //формируем таблицу
	addBoxInfo(jsonPrev);
	//printImg(); //устарело не исп
	printImg(); //Добавление картинок для печати
	printInfo($('.InfoPrint'), jsonPrev.PREV)
	printTable($('.tablePrint'), jsonPrev.PREV)
	postJsonPrev();//тут постобработка объекта
	$("#SaveXml").click(this.SaveXml); //сохраняем в XML старый
	$("#SaveFile").click(this.SaveFile);

	$("#SaveFile").attr("disabled", _savedproject !== "1");

}

//постобработка столешниц:
//связка id столеш и его компонентов
function topPostProc() {
	let index = 0;
	let k = 0; //итерации
	let arrK = [];

	_.forEach(jsonPrev.PREV.RIGHE, function (item) {
		if (item.COD === "TOP") {
			//item.IDBOX = +findTag(item.VAR, "TOPID") + 1;
			//item.ORDINAMENTO = item.IDBOX;
			//console.log(item.COD);
			k++;
			arrK.push(item.MY_UNI_ID);
		}
		item.IDBOX = +item.IDBOX;
	});
}

function getDlin(PREV, cod, findCod, findCod2) {
	var arrRiga = [];
	var rigaIndex = 0;
	//Выбираем только плинтус
	var plint = _.filter(PREV.RIGHE, { 'COD': cod });
	plint.forEach(function (item) {
		//Читаем спецификацию цены
		rDetpresso = item.DETTPREZZO.split("#");
		rDetpresso = _.drop(rDetpresso, 1);//удаляет первый элемент
		//console.log(rDetpresso);
		//console.log(item.VAR);
		rDetpresso.forEach(function (r) {
			let s = [];
			s = r.split(";");
			//console.log(r);
			//console.log(s);
			//if (varVar === item.VAR || (s[1] === findCod || s[1] === findCod2)) { //чувак без вариантов или одинаковым вариантом
			if (s[1] === findCod || s[1] === findCod2) { //чувак без вариантов или одинаковым вариантом
				let riga = CreateRiga();
				riga.COD = cod;
				riga.L = s[7].split(',')[1];
				riga.A = item.A;
				riga.P = item.P;
				riga.PZ = s[6];
				riga.VAR = item.VAR;
				riga.IDBOX = item.IDBOX;
				riga.ORDINAMENTO = item.ORDINAMENTO;//9999
				riga.REAL_L = +item.L; //Дописываю +свойство реальный размер
				arrRiga.push(riga);
			} else {//if (s[1] !== findCod && s[1] !== findCod2) {//чувак с личным вариантом
				let varVar = "";
				if (s.length >= 8) {
					for (let i = 8; i < s.length; i++) { //пересобираю варианты для элемента
						s[i] = s[i].split("=")[0].toUpperCase() + "=" + s[i].split("=")[1];//делаю большими буквами варианты
						if (i === s.length - 1) {
							varVar += s[i];
						} else {
							varVar += s[i] + ";";
						}
					}
				}
				varVar = varVar + ";";
				let riga = CreateRiga();
				riga.COD = s[1];//'ZAGL_COLOR'
				if (typeof s[7] === 'undefined') { riga.L = 0 } else { riga.L = s[7].split(',')[1]; }
				riga.PZ = s[6];
				riga.A = item.A;
				riga.P = item.P;
				riga.VAR = varVar;
				riga.IDBOX = item.IDBOX;
				riga.ORDINAMENTO = item.ORDINAMENTO;
				arrRiga.push(riga);
			}
		});
	});
	//Дописываю в описание длину
	for (let i = 0; i < arrRiga.length; i++) {
		if (arrRiga[i].COD === cod) {
			//console.log(nn.VAR + ' ' + nn.L)
			let kk = findTag(arrRiga[i].VAR, "MYNAME");
			arrRiga[i].VAR = findTagReplace(arrRiga[i].VAR, "MYNAME", kk + ", Д" + arrRiga[i].L);
		}
	}
	//console.log(arrRiga);
	//Добавляем новые строки и удаляем старые
	if (arrRiga.length > 0) {
		//Удаляем плинтус 
		for (let key in PREV.RIGHE) {
			//Номер строки 
			rigaIndex = _.parseInt(key.replace('RIGA', ''));
			if (PREV.RIGHE[key].COD === cod) {
				delete PREV.RIGHE[key];
			}
		}
		rigaIndex++;
		arrRiga.forEach(function (r) {
			PREV.RIGHE['RIGA' + rigaIndex] = r;
			rigaIndex++;
		});
	}

	//ПОСТОБРАБОТКА
	//Цоколя
	soclePostProc(arrRiga);
	//console.log(arrRiga);
}

//Постобработка цоколя
function soclePostProc(arrRiga) {
	//console.log(arrRiga);
	for (let i = 0; i < arrRiga.length; i++) {
		if (arrRiga[i].COD === "SOCLE") {
			let myarticle = findTag(arrRiga[i].VAR, "ARTICLE"); //беру значение тега
			myarticle = myarticle + '\\' + arrRiga[i].L / 1000 + "m"; // \
			arrRiga[i].VAR = arrRiga[i].VAR + ";MYARTICLE=" + myarticle;//и добавляю в варианты
			arrRiga[i].VAR = arrRiga[i].VAR + ";SOCLE_ART=" + myarticle;
		}
	}
	return arrRiga;
}

function topLOut(L_In, variant) { //вывод L_Out длин для 1с для столешек
	let topManuf = findTag(variant, "TOP_MANUFACTURER");
	let topFormCod = findTag(variant, "TOP_FORM_COD");
	let topForm = findTag(variant, "RADIUS");
	let topKromka = findTag(variant, "KROMKA");
	let top_find_l = findTag(variant, "TOP_FIND_L"); //из проги беру размер
	//console.log(top_find_l);
	let L_Out = 0;
	if (topFormCod === "01" || topForm === "Прямоуголная") {
		//console.log(topFormCod);
		if (topManuf === "SOYUZ") {
			if (topKromka === "ABS") {
				L_Out = L_In;
			} else {
				if (L_In == 3000) {
					L_Out = 3050;
				} else if (L_In == 4150) {
					L_Out = 4200;
				} else {
					L_Out = L_In;
				}
			}
		} else if (topManuf === "KEDR") {
			if (L_In == 3000) {
				L_Out = 3050;
			} else if (L_In === 4000) {
				L_Out = 4100;
			} else {
				L_Out = L_In;
			}
		} else if (topManuf === "SLOTEX" || topManuf === "SLOTEXKL") {
			if (topKromka === "ABS") {
				L_Out = L_In;
			} else {
				if (L_In == 3000) {
					L_Out = 3050;
				} else if (L_In == 4150) {
					L_Out = 4200;
				} else {
					L_Out = L_In;
				}
			}
		} else if (topManuf === "SUMSUNG" || topManuf === "HIMACKS") {
			if (L_In == 3000) {
				L_Out = 3680;
			} else if (L_In == 2250) {
				L_Out = 2760;
			} else if (L_In == 1500) {
				L_Out = 1840;
			} else if (L_In == 750) {
				L_Out = 920;
			}
		}
	} else { //фигурные
		if (topManuf === "SLOTEXKL") {
			if (L_In == 3000) {
				L_Out = 3050;
			} else if (L_In == 4150) {
				L_Out = 4200;
			} else {
				L_Out = L_In;
			}
		} else if (topManuf === "SUMSUNG" || topManuf === "HIMACKS") {
			if (L_In == 3000) {
				L_Out = 3680;
			} else if (L_In == 2250) {
				L_Out = 2760;
			} else if (L_In == 1500) {
				L_Out = 1840;
			} else if (L_In == 750) {
				L_Out = 920;
			}
		} else {
			L_Out = top_find_l; //спорный момент
		}
	}
	return L_Out;
}

function topPOut(P_In, variant) {
	let topManuf = findTag(variant, "TOP_MANUFACTURER");
	let topFormCod = findTag(variant, "TOP_FORM_COD");
	let topForm = findTag(variant, "RADIUS");
	let topKromka = findTag(variant, "KROMKA");
	let top_find_p = findTag(variant, "TOP_FIND_P"); //из проги беру размер
	let P_Out = P_In;

	if (topManuf === "KEDR") {
		P_Out = top_find_p;
	}

	return P_Out;
}

function SaveXml() {
	var xml = '<?xml version="1.0"?>' + x2js.json2xml_str(jsonPrev);
	//xml = encodeCP1251(xml);
	var blob = new Blob([xml], { type: "text/plain" });
	saveAs(blob, "express.xml");
}

function SaveFile() {

	var xml = '<?xml version="1.0"?>' + x2js.json2xml_str(jsonPrev);
	var data = {
		expresshtml: b64EncodeUnicode(document.documentElement.innerHTML),
		expressxml: b64EncodeUnicode(xml)
	};
	_wsport = _wsport === "" ? "51278" : _wsport;

	var parmstr = b64EncodeUnicode(document.documentElement.innerHTML) + ";" + b64EncodeUnicode(xml);

	$.ajax({
		url: "http://localhost:" + _wsport,
		type: "POST",
		/*data: JSON.stringify(data),*/
		data: parmstr,
		success: function (data) { },
		error: function (data) { }
	});
}

function b64EncodeUnicode(str) {
	return btoa(
		encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(
			match,
			p1
		) {
			return String.fromCharCode("0x" + p1);
		})
	);
}

//Добавляю страницы с инфой для печали
function addBoxInfo(jsonPrev) {
	let box = []; //корпуса
	//console.log(jsonPrev);
	//Перебираю строки по <IDBOX> кроме длинномеров
	//var plint = _.filter(PREV.RIGHE, { 'cod': cod });
	let i = 1;
	let lastIdbox = 0;
	let t = "";
	_.forEach(jsonPrev.PREV.RIGHE, function (item) {
		//console.log(item);
		let korpusname = findTag(item.VAR, 'korpusname');
		let myName = findTag(item.VAR, 'MYNAME');
		if (item.IDBOX == lastIdbox) {//в этом <IDBOX>
			t += '<span class="classIndent">' + item.DES + ' ' + myName + '</span><br>';
		} else {//переход к след. <IDBOX>
			t += '<span>' + i + '. ' + item.DES + ' ' + korpusname + myName + '</span><br>';
			i++;
		}

		lastIdbox = item.IDBOX;
	})
	pageInfo = t;
	pageInfo = '\
		<div class="wrapPageInfo">\
			'+ pageInfo + '\
			<table class="titleBlock">\
				<tr><td>Заказ № '+ JsNumero + '</td> <td>Дата: ' + JsData + '</td></tr>\
				<tr><td colspan="2">Покупатель: '+ JsBFio + '</td></tr>\
				<tr><td colspan="2">Дизайнер: '+ JsFioDis + '</td></tr>\
				<tr><!--<td colspan="2">Примечание: '+ JsRif + '</td></tr>-->\
			</table>\
		</div>\
		';

	//document.querySelector('#footer').innerHTML = pageInfo;
}

//Таблица//
function showTd(i) {
	document.getElementById(i).style.display = "block";
}

function myTable() {
	var n = 0;

	dlinnomer = "TOP, PLINTUS, SOCLE, KARNIZ, KARNIZN, BALUSTRADA";
	n = 1;
	let OrdinaOld = 0;
	for (let key in jsonPrev.PREV.RIGHE) {
		//вычисляю box

		let a = jsonPrev.PREV.RIGHE[key];
		//console.log(a.COD);
		//if (OrdinaOld !== a.ORDINAMENTO) { trStyle = "mainT" } else { trStyle = "" };
		if (a.MODELLO !== "") { trStyle = "mainT" } else { trStyle = "" };//надеюсь в Code всегда будет не пусто			
		addTr(n, trStyle, a);
		OrdinaOld = a.ORDINAMENTO;
		n++;
	}

	document.getElementById("myTbl").innerHTML = '<table id="mytable">\
			<thead>\
			<tr>\
				<th class="styleTableHead" width="15px">№</th>\
				<th class="styleTableHead" width="50px">Код</th>\
				<th class="styleTableHead" width="525px">Наименование</th>\
				<th class="styleTableHead" width="20px"><span>Количество</span></th>\
				<th class="styleTableHead" width="20px"><span>Артикул</span></th>\
				<!-- <th class="styleTableHead HiddenCol" width="10px"></th> -->\
			<tr>\
			</thead>\
			'+ tableTr + '\
			</table>\
			';
	tableTr = "";
}

function addTr(n, trStyle, key) {
	let myName = findTag(key.VAR, 'MYNAME');
	let korpusname = findTag(key.VAR, 'korpusname');
	//if (key === "TOP") { myName += ' ' + key.L }
	let myArticle = findTag(key.VAR, 'MYARTICLE');
	//console.log(myArticle);
	tableTr += '\
		<tr id="riga' + n + '" class="' + trStyle + '">\
			<td class="number" width="15px">' + n + '</td>\
			<td class="code" width="50px">' + key.COD + '</td>\
			<td class="des" width="525px" onload="loadDes()">\
				' + key.DES + ' ' + myName + korpusname + tempText + '\
			</td>\
			<td class="count" width="20px">' + key.PZ + '</td>\
			<td class="count" width="20px">' + myArticle + '</td>\
		</tr>\
	';
}

//В связке с CreateRiga()
//копирование в новую строку
function SetRiga(newRiga, oldRiga) {
	for (let key in oldRiga) {
		newRiga[key] = oldRiga[key];
	}
	return newRiga;
}

//TODO: 12/12/2019 yurii
//Добавление предпросмотра для формы.
//
function drawImgPreview(elm, ImgList) {
	var htmltext = "";
	//Перебираем массив изображений
	for (var i = 0; i < ImgList.length; i++) {
		if (ImgList[i] && ImgList[i] !== "") {
			htmltext += '<div class="imgBox">\
			<img src="..\\..\\'+ ImgList[i] + '">\
			<input type="checkbox" checked class="checkImgI" onclick="printImg()" value="..\\..\\'+ ImgList[i] + "?v=" + Math.random().toString(36).substr(2, 9) + '">\
		</div>';
		}
	}
	//добавляем изображения на форму.
	if (htmltext !== "") {
		elm.html(htmltext);
	}
}

//TODO: 16/12/2019 yurii
//Добавление картинок для печати
//
function printImg() {
	var ImgChekElement = $('.checkImgI')
	var Cod1S = c_cod;
	var lblData = jsonPrev.PREV.TESTA.DATA;
	var htmltext = "";
	for (var n = 0; n < ImgChekElement.length; n++) {
		if (ImgChekElement[n].checked) {
			if (n == 0) {
				htmltext += '\
				<div class="wrapPageI">\
				    <div class = "imgArg">\
				    <div class = "txtCentr">Приложение №3 к договору № '+ Cod1S + ' от ' + lblData + '</div>\
					<img height="99%" src="'+ ImgChekElement[n].value + '">\
					</div>\
				</div>';
			} else {
				htmltext += '\
				<div class="wrapPageI">\
				    <div class = "imgArg">\
					<img height="99%" src="'+ ImgChekElement[n].value + '">\
					</div>\
				</div>';
			}
		}
	}
	$('.imgPrint').html(htmltext);

}

//TODO: 16/12/2019 yurii
//Добавление информации по заказу
function printInfo(elm, PREV) {
	var lblNumber = PREV.TESTA.VAR["C_COD"];//PREV.TESTA.NUMERO;
	var lblData = PREV.TESTA.DATA;
	var lblDis = PREV.TESTA.VAR.FIODIS.split(',')[2];
	var lblPokup = PREV.TESTA.VAR.B_FIO;
	lblDis = dis_fio;
	lblNumber = c_cod;
	//console.log(c_cod);

	var htmltext = '<div><table class="titleBlock">\
				<tr><td width = "150px">Дата: '+ lblData + '</td> <td colspan="2">№ Заказа: ' + lblNumber + '</td><td width = "150px">Версия: ' + dateversion + '</td></tr>\
				<tr><td>Дизайнер: </td><td colspan="3">'+ lblDis + '</td> </tr>\
				<tr><td colspan="4">Схема проекта (эскиз) мне понятна,мною проверена. С размерами,расположением,декоративными элементами и цветом комплектующих,в том числе:\
                  столешницы,фальш-панели,фасадов (и их фрезеровкой) ознакомлен и согласен</td></tr>\
				<tr><td>Покупатель: </td><td width = "300px">'+ lblPokup + '</td><td width = "100px">Подпись </td><td> </td></tr>\
			</table></div>';
	elm.html(htmltext);

}

//TODO: 16/12/2019 yurii
//
function printTable(elm, PREV) {
	var htmltext = '<div class="lblSpec">Перечень позиций</div>'
	htmltext += '<div><table class="tblInfo">\
	  <thead>\
	  <th>Описание</th>\
	  <th width="10%">Размеры</th>\
	  <th width="5%">Кол-во</th>\
	  </thead>';
	var OrdinaOld, Note; //СОРТИРОВКА
	var flTop = 0;
	let arr = [];
	let idArrTop = 0;
	let ordiniHtmlText;
	let once = 0;

	_.forEach(PREV.RIGHE, function (item) {
		if (item.COD !== "FORTOP") { //WORK
			var myName = findTag(item.VAR, 'MYNAME');
			var korpusname = findTag(item.VAR, 'korpusname');
			let once = 0;
			//console.log(item.ORDINAMENTO + ' ' + item.COD);
			if ((OrdinaOld !== item.ORDINAMENTO) && (item.ORDINAMENTO !== '9999'
				&& item.ORDINAMENTO !== 9999) && typeof (item.ORDINAMENTO) !== 'undefined'
				&& item.COD !== "TOP" && item.COD !== "KROMKA") {

				//Примечание
				if ((item.ORDINAMENTO !== '9999' && item.ORDINAMENTO !== 9999) && (Note && Note !== "")) {
					htmltext += '<tr>\
				  <td class="tdBold">Примечание: '+ Note + '</td>\
				  <td></td>\
				  <td></td>\
			     </tr>';
				}

				//Позиция
				htmltext += '<tr>\
				  <td class="tdBold">Позиция '+ item.ORDINAMENTO + '</td>\
				  <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
				  <td class="txtCentr">'+ item.PZ + '</td>\
				 </tr>';
				//console.log(item.ORDINAMENTO + ", " + item.COD);

				//если заглавного нет
				if ((item.ELMC == 1 || item.COD === "TOP") || item.COD === "PLANKA"
					|| item.COD === "WORK") {
					htmltext += '<tr>\
					  <td>'+ item.DES + ' ' + myName + korpusname + '</td>\
					  <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
					  <td class="txtCentr">'+ item.PZ + '</td>\
					</tr>'
					//console.log(`${item.COD} если заглавного нет`);
				}

				Note = item.NOTE;
			} else {
				if ((item.ORDINAMENTO === '9999' || item.ORDINAMENTO == 9999 ||
					typeof (item.ORDINAMENTO) === 'undefined') && flTop == 0) {
					htmltext += '<tr>\
				  <td class="tdBold">Длинномеры:</td>\
				  <td></td>\
				  <td></td>\
			    </tr>';
					flTop = 1;
				}

				//остальные все позиции//
				//if (item.COD !== "TOP") {
				htmltext += '<tr>\
				  <td>'+ item.DES + ' ' + myName + korpusname + '</td>\
				  <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
				  <td class="txtCentr">'+ item.PZ + '</td>\
				</tr>'
				//}
			}
			OrdinaOld = item.ORDINAMENTO;
		}
	});

	if (PREV.TESTA.VAR.NOTE && PREV.TESTA.VAR.NOTE !== "") {
		htmltext += '<tr>\
			  <td class="tdBold">Примечание: '+ PREV.TESTA.VAR.NOTE + '</td>\
			  <td></td>\
			  <td></td>\
		     </tr>';
	}
	htmltext += '</table></div>';
	htmltext += '<div class="ftrSpec">Перечень позиций представляет собой пример сборки из комплектующих, не является спецификацией к договору, не отменяет и не изменяет Спецификацию.</div>'
	elm.html(htmltext);
}

//постобработка JsonPrev
function postJsonPrev() {

	//prevReNumber();

	//удаляю VarArr
	for (let key in jsonPrev.PREV.RIGHE) {
		//console.log(jsonPrev.PREV.RIGHE[key].VarArr);
		delete jsonPrev.PREV.RIGHE[key].VarArr;
	}
	//console.log(jsonPrev.PREV.RIGHE);

	//заменяю #nbsp на пробелы
	for (let key in jsonPrev.PREV.RIGHE) {
		let item = jsonPrev.PREV.RIGHE[key];
		//jsonPrev.PREV.RIGHE[key];
		//console.log(key);
		for (let k in jsonPrev.PREV.RIGHE[key]) {
			if (k === "DETTPREZZO" || k === "VAR") {
				let str = jsonPrev.PREV.RIGHE[key][k];
				//console.log(k);
				str.replace(/\&nbsp\;/gi, ' ');
			}
		}

		// let service = findTag(item.VAR, "SERVICE");
		// if (service === "delete") {
		// 	delete jsonPrev.PREV.RIGHE[key];
		// }
	}
}
