/*
    Методы основного объекта
*/
import { RIGHE } from "..";
import { RigaStatus } from "@js/riga/rigaStatus.js";

export default class Riga {
    constructor(RIGHE) {
    }

    /* ФУНКЦИИ ДЛЯ RIGHE */
    log() {
        console.log(this);
    }

    init() {
        let forI = 1;

        for (const key in this) {
            let item = this[key];
            //уникальный номер
            item.MY_UNI_ID = item.COD + "_" + forI;
            //console.log(item.COD, item.MY_UNI_ID);

            item.COD = item.COD.toUpperCase();
            item.IDBOX = Number(item.IDBOX);
            item.A = Number(item.A);
            item.L = Number(item.L);
            item.P = Number(item.P);
            item.ELMC = Number(item.ELMC); //кол-во всех элем. в одной группе id
            item.IDUNICO = Number(item.IDUNICO);
            item.QTA = Number(item.QTA);
            item.PZ = Number(item.PZ);
            item.ORDINAMENTO = Number(item.ORDINAMENTO);
            item.PARENTRIGA = key;
            //item.STATUS = RigaStatus.READY;

            forI++;

            //буква ю
            /*item.VAR = item.VAR.replace('0XFE', 'Ю');
            item.DETTPREZZO = item.DETTPREZZO.replace('0XFE', 'Ю');
            item.VARIANTI = item.VARIANTI.replace('0XFE', 'Ю');
            item.VAR = item.VAR.replace('0xfe', 'ю');
            item.DETTPREZZO = item.DETTPREZZO.replace('0xfe', 'ю');
            item.VARIANTI = item.VARIANTI.replace('0xfe', 'ю');*/

            //Преобразуем строку варинтов в массив [ключ,значение]
            /*item.VARS = item.VAR.split(';').map(function (item) {
                return item.split('=');
            });*/
            item.VARS = this.setVars(item.VAR);

            //using to uppercase
            item.VAR = item.VARS.map((currentValue, index, array) => {
                return currentValue.map((cur, i, arr) => { return cur }).join('=')
            }).join(';');

            //процедура установки id столешки из тэга
            if (item.COD === "TOP") {
                item.IDBOX = Number(+this.getTag(item, "TOPID")) + 1;
            }

            //полное описание
            if (item.COD === "WORK") {
                //item.DES
            }
            item.MY_DES = `${item.DES} ${item.COD === "WORK" ? item.NOTE : ""} ${this.getTag(item, "MYNAME")} ${this.getTag(item, "KORPUSNAME")}`
        }
    }

    addNew(riga, newIndex = 0) { //!!!error
        if (!riga) {
            console.log(`У addNew отсутствует riga с индексом ${newIndex}`);
        } else {
            /*let tempIndex = 10001;
            let b = 1;

            for (let key in this) {
                let thisIndex = _.parseInt(key.replace('RIGA', ''));
                if (thisIndex == newIndex) {
                    this['RIGA' + tempIndex] = riga;
                    tempIndex++;
                }
                this['RIGA' + tempIndex] = this[key];
                delete this[key];
                tempIndex++;
            }
            for (let key in this) {
                this['RIGA' + b] = this[key];
                delete this[key];
                b++;
            }*/
            this.RIGA9999 = riga;
            this.replace(riga, newIndex);
        }
    }

    //в конец списка
    addNewToEnd(riga) {
        let newIndex = this.lastNumber() + 1;
        this['RIGA' + newIndex] = riga;
    }

    //добавить после riga с MY_UNI_ID !!!error
    addNewByUniId(riga, MY_UNI_ID) {
        if (!riga) {
            console.log(`У addNewByUniId отсутствует riga с MY_UNI_ID ${MY_UNI_ID}`);
        } else {
            this.RIGA999 = riga;
            let newIndex;

            for (const key in this) {
                if (this.hasOwnProperty(key)) {
                    if (this[key].MY_UNI_ID == MY_UNI_ID) newIndex = 1 + _.parseInt(key.replace('RIGA', ''));
                }
            }
            if (newIndex) {
                this.replace(riga, newIndex);
            } else {
                console.log(`addNewByUniId: newIndex: ${newIndex} не найден`);
            }
        }
    }

    replace(riga, newIndex) {
        let tempIndex = 10001;
        let b = 1;
        let oldIndex = 0;
        let rigaUniId = riga.MY_UNI_ID;
        let isNewIndex = false;

        for (let key in this) {
            let thisIndex = _.parseInt(key.replace('RIGA', ''));
            if (rigaUniId == this[key].MY_UNI_ID) {
                oldIndex = _.parseInt(key.replace('RIGA', ''));
                //console.log("replace: oldIndex", oldIndex);
            }
            if (thisIndex == newIndex) isNewIndex = true;
        }
        if (oldIndex < newIndex) newIndex++; //!important

        if (oldIndex != newIndex) {
            for (let key in this) {
                let thisIndex = _.parseInt(key.replace('RIGA', ''));
                //console.log("replace: thisIndex", thisIndex, this[key].COD);
                //delete this['RIGA' + oldIndex];

                if (thisIndex == newIndex) { //1

                    this['RIGA' + tempIndex] = riga;
                    //console.log("replace: newIndex", newIndex);
                    tempIndex++;
                }
                //искомый индекс отсутствует
                if (!isNewIndex && thisIndex > newIndex) {
                    this['RIGA' + tempIndex] = riga;
                    //console.log("replace: when newIndex not found, newIndex:", newIndex);
                    tempIndex++;
                    isNewIndex = true;
                } else {
                    this['RIGA' + tempIndex] = this[key];
                }
                //delete this[key];
                tempIndex++;
            }
            delete this['RIGA' + oldIndex];

            for (let key in this) {
                let num = _.parseInt(key.replace('RIGA', ''));
                if (num > 10000) {
                    this['RIGA' + b] = this[key];
                    delete this[key];
                    b++;
                }
            }
        }
    }

    replaceByUniId(riga, uniId) { //!!! ???
        let newIndex;
        for (const key in this) {
            if (this.hasOwnProperty(key)) {
                if (this[key].MY_UNI_ID == uniId) {
                    newIndex = _.parseInt(key.replace('RIGA', ''));
                }
            }
        }
        if (newIndex && riga) {
            this.replace(riga, newIndex);
        } else if (!riga) {
            console.log("replaceByUniId:", "riga=", riga, "uniId=", uniId);
        } else {
            console.log("replaceByUniId:", "newIndex=", newIndex, "uniId=", uniId);
        }
    }

    delByUniId(arrUniId = []) {
        let isDel = false;
        if (arrUniId) {
            if (arrUniId && !(Array.isArray(arrUniId))) arrUniId = arrUniId.toString().split();
            for (let key in this) {
                if (arrUniId.includes(this[key].MY_UNI_ID.toString())) {
                    //console.log(arrUniId);
                    isDel = true;
                    delete this[key];
                }
            }
            if (!isDel) console.log(`${arrUniId} not deleted`)
            //this.renumber();
        }
    }

    delByCod(arrCod = []) {
        if (arrCod && !(Array.isArray(arrCod))) arrCod = [arrCod];//arrCod.toString().split();
        for (let key in this) {
            if (arrCod.includes(this[key].COD.toString())) {
                delete this[key];
            }
        }
        //this.renumber();
    }

    create() {
        let obj = Object.create(null);
        obj = {
            A: 0,
            COD: "",
            CODDX: "",
            CODNEUTRO: "",
            DES: "",
            DESNEUTRO: "",
            DETTPREZZO: "",
            FLAGS: "",
            IDBOX: "",
            IDUNICO: "",
            L: 0,
            MODELLO: "",
            NOTE: "",
            ORDINAMENTO: 1, //1-не длинномер, 9999-длинномер
            P: 0,
            PESOL: "",
            PESON: "",
            PR1: "",
            PR2: "",
            PZ: "",
            QTA: "",
            SC1: "",
            SC2: "",
            SC3: "",
            SC4: "",
            VAR: "",
            VARIANTI: "",
            VOLUME: "",
            MY_UNI_ID: this.setUniId(),
            MY_DES: "",
            VARS: [],//this.VAR ? setVars() : []
            PARENTRIGA: '',
        }
        return obj
    }

    createBased(oldRiga) { //такой же MY_UNI_ID
        let newRiga = this.create();
        for (let key in oldRiga) {
            if (Array.isArray(oldRiga[key])) {
                //deep copy for array
                newRiga[key] = _.cloneDeep(oldRiga[key]);
            } else {
                newRiga[key] = oldRiga[key];
            }
        }
        return newRiga;
    }

    findByUniId(uniId) {
        for (let key in this) {
            if (this[key].MY_UNI_ID == uniId) {
                return this[key]
            }
        }
    }

    //return first riga [object]
    findByCod(cod) {
        if (!cod) return undefined;
        for (const key in this) {
            if (this.hasOwnProperty(key)) {
                if (this[key].COD === cod) {
                    return this[key];
                }
            }
        }
    }

    //перенумерация //!!!error
    renumber() {
        let tempIndex = 10001;
        let b = 1;

        for (let key in this) {
            this['RIGA' + tempIndex] = this[key];
            //delete this[key]; //!!!important
            tempIndex++;
        }

        for (let key in this) {
            let num = _.parseInt(key.replace('RIGA', ''));
            if (num > 10000) {
                this['RIGA' + b] = this[key];
                delete this[key];
                b++;
            }
        }
    }

    lastNumber() {
        let rigaIndex = 0;
        for (let key in this) {
            if (rigaIndex < _.parseInt(key.replace('RIGA', ''))) {
                rigaIndex = _.parseInt(key.replace('RIGA', ''));
            }
        }
        //console.log(rigaIndex);
        return rigaIndex
    }

    //группировка с кодом и по вариантам 
    // return new object со ссылками на riga
    group(keyVarGroup, cod) {

        var top = _.filter(this, { 'COD': cod });

        //Группируем по вариантам
        var topGroup = _.groupBy(top, function (item) {
            //подбираем варианты для объединения
            //Фильтруем только нужные варианты
            var GroupKey = item.VARS.filter(function (iVar) {
                return _.indexOf(keyVarGroup, iVar[0]) >= 0
            });
            //Преобразуем в строку <вариант>=<Значение>
            GroupKey = GroupKey.map(function (ikey) {
                return _.join(ikey, '=');
            });
            //объединяем все варинты в троку с разделителем ";"
            return _.join(GroupKey, ';');
        });
        return topGroup;
    }

    //группировка по вариантам с фильтром-условием, не готов
    groupByVar(keyVarGroup, cod, callBack) {
        var top = _.filter(this, { 'COD': cod });

        //Группируем по вариантам
        var topGroup = _.groupBy(top, function (item) {
            //подбираем варианты для объединения
            //Фильтруем только нужные варианты
            var GroupKey = item.VARS.filter(function (iVar) {
                return _.indexOf(keyVarGroup, iVar[0]) >= 0
            });
            //Преобразуем в строку <вариант>=<Значение>
            GroupKey = GroupKey.map(function (ikey) {
                return _.join(ikey, '=');
            });
            //объединяем все варинты в троку с разделителем ";"
            return _.join(GroupKey, ';');
        });
        return topGroup;
    }

    //установить IDBOX у child как у родителя с кодом=cod
    setChildByCod(child, cod) {
        let last;
        for (let key in this) {
            if (this[key].COD === cod) {
                last = this[key];
            }
        }
        if (last) child.IDBOX = last.IDBOX;
    }


    /* МЕТОДЫ ДЛЯ RIGA */
    setUniId() {
        return Math.floor(Math.random() * 100000000).toString()
    }

    //return string
    getTag(riga, tag) {
        if (!riga.VARS) {
            console.log(riga, tag, "--riga.VARS отсутствует");
            return ""
        } else {
            tag = tag.toString()
            /*if (riga.VARS.indexOf(tag) != -1) {
                return riga.VARS.filter(function (o) {
                    return (o[0] === tag)
                })[0][1];
            } else return ""*/
            for (let index = 0; index < riga.VARS.length; index++) {
                const el = riga.VARS[index];
                if (el[0] == tag) return el[1]
            }
            return ""
        }
    }

    setTag(riga, tag, value = "") {
        if (!riga.VARS || !tag) {
            //console.log(riga, tag, "riga.VARS или tag отсутствует");
        } else {
            tag = tag.toString()
            let text = "";
            //in VARS
            let arr = riga.VARS.map(function (item, index, arr) {
                if (item[0] == tag) {
                    text = item[1];
                    item[1] = value;
                }
                return item
            })
            riga.VARS = arr

            //in VAR
            if (text) {
                riga.VAR = riga.VAR.replace(text, value);
            }
        }
    }

    newTag(riga, tag, value) {
        //in VARS
        tag = tag.toString().toUpperCase().trim()
        value = value.toString()
        riga.VAR ? riga.VARS.push([tag, value]) : riga.VARS = [[tag, value]];
        //in VAR
        if (riga.VAR) { riga.VAR = riga.VAR.concat(";" + tag + "=" + value) }
        else { riga.VAR = (tag + "=" + value) }
    }

    //return bool
    hasTag(riga, tag) {
        if (!riga.VARS) {
            console.log(riga, tag, "hasTag: --riga.VARS отсутствует");
        } else {
            if (getTag(riga, tag))
                return true;
        }
        return false;
    }

    //Преобразует DETTPREZZO=text в массив для VARS
    //text (string) DETTPREZZO
    //return array
    setVars(text, splitter = ';') {
        return text.split(splitter).map(function (item) {
            return item.split('=').map(function (itm, i) {
                if (i == 0) itm = itm.trim().toUpperCase(); //value
                return itm
            });
        });
    }
}