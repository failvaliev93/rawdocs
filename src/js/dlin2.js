﻿//второй шанс модуля длинномеров

//суперкласс
function ModuleDlin() {
    this.x = 12;          //тест
    this.oldDlin = [];    //было изначально
    this.autoDlin = [];   //режим авто
    this.manualDlin = []; //режим ручной
    this.id = 0;          //идентификатор
    this.html = "";       //предполагается 
    this.arrL = [];       //список пришедших кусков
    this.activeDlin = "not";//запускаю с с просчетом без изм
}

let myDlin = Object.create(null); //содержит все объекты длин-в

//console.log(a.x);
function test() {
    //console.log(document.querySelector('#pOptOut0'));
}

//вывод всего модуля
function drawModuleDlin() {
    //data = prev;
    //calcDlin();
    let dlinContainer = document.createElement('div');
    dlinContainer.className = "dlinContainer";

    //Длинномеры
    let h2 = document.createElement('h2');
    h2.textContent = "Длинномеры:";
    dlinContainer.appendChild(h2);

    let dlinContent = document.createElement('div');
    dlinContent.className = "dlinContent";
    dlinContainer.appendChild(dlinContent);

    //элементы группы
    let dlinElem = grupDlin();//elementDlin();
    for (let i = 0; i < dlinElem.length; i++) {
        dlinContent.appendChild(dlinElem[i]);
    }

    //кнопка
    let button = document.createElement('button');
    button.id = button.className = "bAddChanges";
    button.onclick = test;
    button.textContent = "Готово";
    //button.disabled = true;
    dlinContent.appendChild(button);

    let clear = document.createElement('div');
    clear.className = "clear";
    dlinContent.appendChild(clear);

    document.querySelector('body').appendChild(dlinContainer);
}

//группирую длин-ы и для каждого рисую elementDlin()
function grupDlin() {
    const keyVarGroup = ['TOP_MANUFACTURER', 'STOLESH_MAT', 'TOLSHINA', 'KROMKA', 'RADIUS', 'TWO_SIDE_COLOR'];

    var top = _.filter(jsonPrev.PREV.RIGHE, { 'COD': 'TOP' });
    //Перебираем массив столешниц
    top = top.map(function (item) {
        //Преобразуем сторку вариантов в массив
        item.VarArr = stringToMap(item.VAR);
        //console.log(item.REAL_L);
        return item;
    });
    //Группируем столешницы по вариантам
    var topGroup = _.groupBy(top, function (item) {
        //подбираем варианты для объединения столшниц
        //Фильтруем только нужные варианты
        var GroupKey = item.VarArr.filter(function (iVar) {
            return _.indexOf(keyVarGroup, iVar[0]) >= 0
        });
        //Преобразуем в строку <вариант>=<Значение>
        GroupKey = GroupKey.map(function (ikey) {
            return _.join(ikey, '=');
        });
        //объединяем все варинты в троку с разделителем ";"
        return _.join(GroupKey, ';');
    });
    //console.log(topGroup);

    //записываю 
    let n = 0;   //id
    let allElem = []; //html длинномеров, начинается с 0
    for (let key in topGroup) {
        let sumLReal = 0;
        let dlin = new ModuleDlin();
        dlin.id = n;
        //Без изменений и ручной//
        for (let i = 0; i < topGroup[key].length; i++) {
            dlin.manualDlin.push(SetRiga(CreateRiga(), topGroup[key][i]));
            dlin.oldDlin.push(SetRiga(CreateRiga(), topGroup[key][i]));
            sumLReal += topGroup[key][i].REAL_L;
        }
        myDlin['dlin' + n] = dlin;
        allElem[n] = elementDlin(n);
        n++;
        //console.log(sumLReal);

        //Авто//
        let arrL = findTag(topGroup[key][0].VAR, "TOP_L").split('*');
        let arrLCount = [];
        let newArrDlin = [];
        let countI = 0;
        for (let i = 0; i < arrL.length; i++) {
            arrL[i] = +arrL[i]; //перевожу в числа
            arrLCount[i] = 0;
        }
        arrL.sort(compareNumeric); //сортирую разрешенные длины от меньшего

        for (let i = arrL.length; i >= 0; i--) {
            if (arrL[i] <= sumLReal) { //кусок влезает
                countI = Math.floor(sumLReal / arrL[i]);
                sumLReal -= arrL[i] * countI;
                arrLCount[i] = countI;
                //console.log(i);
            }
        }
        if (sumLReal > 0) { //остался кусочек 
            arrLCount[0] += 1;
        }
        //console.log(arrLCount);

        //массив новых кусков
        for (let i = 0; i < arrLCount.length; i++) {
            if (arrLCount[i] > 0) {
                let riga = SetRiga(CreateRiga(), topGroup[key][0]);
                //console.log(topGroup[key][0]);
                riga.L = arrL[i]; //длина
                riga.PZ = arrLCount[i]; //количество               

                //меняю в описание длину
                let newName = findTag(riga.VAR, "MYNAME");
                newName = newName.replace(topGroup[key][0].L, riga.L);
                riga.VAR = findTagReplace(riga.VAR, "MYNAME", newName);
                //console.log(arrDlin[0].L + '; ' + riga.L);
                //console.log(riga);
                newArrDlin.push(riga);
            }
        }
        //console.log(newArrDlin);

        //Добавляем новые строки и удаляем старые
        let rigaIndex = 0;
        if (newArrDlin.length > 0) {
            //Удаляем  
            for (let key in jsonPrev.PREV.RIGHE) {
                //Номер строки 
                rigaIndex = _.parseInt(key.replace('RIGA', ''));
                if (jsonPrev.PREV.RIGHE[key].COD === "TOP") {
                    delete jsonPrev.PREV.RIGHE[key];
                }
            }
            rigaIndex++;
            newArrDlin.forEach(function (r) {
                jsonPrev.PREV.RIGHE['RIGA' + rigaIndex] = r;
                rigaIndex++;
            });
        }

        dlin.arrL = arrL;
    }
    console.log(myDlin);

    return allElem;
}


//рисую по элементу из группы
function elementDlin(id) {
    //контейнер 1 элемента
    let dlinElem = document.createElement('div');
    dlinElem.className = "dlinElem";
    dlinElem.id = "dlinElem" + id;

    let gorLine = document.createElement('div');
    gorLine.className = "gorLine";
    dlinElem.appendChild(gorLine);

    let dlinHead = document.createElement('div');
    dlinHead.className = "dlinHead";
    dlinElem.appendChild(dlinHead);
    dlinHead.textContent = "Столешницы. Просчёт:";

    //просчет по умолч-ю Без изменений //
    let selDlin = document.createElement('select');
    selDlin.size = 1;
    selDlin.id = "selCalc" + id;
    selDlin.onchange = onChangecalcDlin; //ивент
    let optionAuto = document.createElement('option');
    optionAuto.value = "auto";
    optionAuto.textContent = "Автоматически";
    //selDlin.add(optionAuto); //!!! нету авто
    let optionNot = document.createElement('option');
    optionNot.value = myDlin['dlin' + id].activeDlin;
    optionNot.textContent = "Без изменений";
    optionNot.selected = true; //по умолч-ю
    selDlin.add(optionNot);
    let optionManual = document.createElement('option');
    optionManual.value = "manual";
    optionManual.textContent = "Ручной";
    selDlin.add(optionManual);
    dlinHead.appendChild(selDlin);
    //console.dir(selDlin);

    //описание этой столешки
    let mynameDlin = document.createElement('div');
    mynameDlin.textContent = myDlin['dlin' + id].oldDlin[0].VarArr[0][1].split("Т")[0];
    mynameDlin.className = "mynameDlin";
    dlinHead.appendChild(mynameDlin);

    //таблица
    let tab = document.createElement('div');
    tab.className = "tab";
    dlinElem.appendChild(tab);

    //найдено
    let tabFind = document.createElement('div');
    tabFind.className = "tabFind";
    tab.appendChild(tabFind);
    tabFind.textContent = "Найдено:";
    let tabFindIn = document.createElement('div');
    tabFindIn.className = "tabFindIn";
    tabFind.appendChild(tabFindIn);

    //куски длин в найдено
    let sumL = 0;
    for (key in myDlin['dlin' + id].oldDlin) {
        let classCss = "odd";               //цвет фона четной
        if (key % 2 == 0) classCss = "even";  //не четная

        let x = myDlin['dlin' + id].oldDlin;
        let p = document.createElement('p');
        p.className = classCss;
        p.textContent = x[key].PZ + " длиной " + x[key].REAL_L + '(' + x[key].L + ')';
        tabFindIn.appendChild(p);

        sumL += x[key].L;
    }

    // Оптимизировано
    let tabOpt = document.createElement('div');
    tabOpt.className = "tabOpt";
    tab.appendChild(tabOpt);
    tabOpt.textContent = "Оптимизировано:";
    let tabOptIn = document.createElement('div');
    tabOptIn.className = "tabOptIn";
    tabOptIn.id = "tabOptIn" + id;
    tabOpt.appendChild(tabOptIn);

    //итого
    let total = document.createElement('div');
    total.className = "total";
    dlinElem.appendChild(total);

    //итого найдено
    let tabFindOut = document.createElement('div');
    tabFindOut.className = "tabFindOut";
    total.appendChild(tabFindOut);
    let pFindOut = document.createElement('p');
    pFindOut.className = "pFindOut";
    pFindOut.id = "pFindOut" + id;
    pFindOut.textContent = 'Итого: общая длина ' + sumL;
    tabFindOut.appendChild(pFindOut);

    //итого Оптимизировано
    let tabOptOut = document.createElement('div');
    tabOptOut.className = "tabOptOut";
    total.appendChild(tabOptOut);
    let pOptOut = document.createElement('p');
    pOptOut.className = "pOptOut";
    pOptOut.id = "pOptOut" + id;
    pOptOut.hidden = true;
    pOptOut.textContent = 'Итого: общая длина ';
    tabOptOut.appendChild(pOptOut);
    //цифра
    let spanOptOut = document.createElement('span');
    spanOptOut.className = "tabFindOutL";
    spanOptOut.id = "tabFindOutL" + id;
    spanOptOut.textContent = 0;
    pOptOut.appendChild(spanOptOut);

    let clear = document.createElement('div');
    clear.className = "clear";
    total.appendChild(clear);

    //исходя из просчета в Оптимизировано.
    //при переключении ивент запускает эту ф-ю
    calcDlin(selDlin.value, id, tabOptIn, spanOptOut);

    return dlinElem;
}

//рисую в Оптимизировано исходя из выбора просчета
//value[text] - значение из просчета
//id[number] - номер группы
//tabOptIn[object] - в него будет помещен результат
//tabFindOutL[object] - так же показываю сумму
function calcDlin(value, id, tabOptIn, tabFindOutL) {
    //console.log(tabFindOutL);
    let sumL = 0;

    if (value === "auto") {
        let x = myDlin['dlin' + id].autoDlin;
        for (key in x) {
            let classCss = "odd";               //цвет фона четной
            if (key % 2 == 0) classCss = "even";  //не четная

            let p = document.createElement('p');
            p.className = classCss;
            p.textContent = x[key].PZ + " длиной " + x[key].REAL_L + '(' + x[key].L + ')';
            tabOptIn.appendChild(p);
            sumL += x[key].L;
        }

    } else if (value === "not") {//без изменений
        let x = myDlin['dlin' + id].oldDlin;
        for (key in x) {
            let classCss = "odd";               //цвет фона четной
            if (key % 2 == 0) classCss = "even";  //не четная

            let p = document.createElement('p');
            p.className = classCss;
            p.textContent = x[key].PZ + " длиной " + x[key].REAL_L + '(' + x[key].L + ')';
            tabOptIn.appendChild(p);
            sumL += x[key].L;
        }

    } else if (value === "manual") { //ручной

    }

    tabFindOutL.textContent = "Итого: общая длина " + sumL;
    myDlin['dlin' + id].activeDlin = value;
}

//ивент на изменение просчета
function onChangecalcDlin() {
    //console.log(event.target);
    let id = +event.target.id.split("selCalc")[1];
    let tabOptIn = document.getElementById("tabOptIn" + id);
    let tabFindOutL = document.getElementById("pFindOut" + id);

    calcDlin(event.target.value, id, tabOptIn, tabFindOutL);
    //console.log(id);
}











