/*
    Реализация кнопки Узнать цену
*/

/*
    window.md1 = metadata
    window.xhr = resObj
*/

import { preSave } from '@js/func.js'
import { jsonPrev, RIGHE } from '../index.js'
import * as $ from 'jquery'
import X2JS from '@js/xml2json.js'
import mui from '@js/mui.js'

let token = '132';//'d80244a3568c442a8511ec84b1cfa7e5';
let orderNum = '24234';
const priceUrl = 'http://tsrv1.kuhni-express.ru/express/hs/3cad/calculation/';
let xhr;

export function CheckPrice() {
    let btn = document.createElement('button');
    btn.id = "checkPrice";
    btn.className = "mui-btn mui-btn--raised mui-btn";
    btn.innerText = "Проверить цену";

    let btnSave = document.querySelector("#SaveFile");
    btnSave.parentNode.appendChild(btn);

    let json = JSON.stringify(preSave(jsonPrev));

    let meta = getMetadata(METADATA);
    window.md1 = meta;
    token = meta.Metadata.Destination.SecurityToken;
    orderNum = c_cod;

    btn.onclick = e => {
        sendJSON(json, `${priceUrl}${orderNum}?token=${token}`);
        showModalWindow();
        startLoading();
    }
}

function getMetadata(xmlDoc) {
    var x2js = new X2JS();
    return x2js.xml_str2json(xmlDoc);
}

// пример callback
function httpGetAsync(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        console.log('status:', xmlhttp.status);
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true, 'download', 'download'); // true for asynchronous 
    xmlHttp.send(null);
}

function sendJSON(data, url) {
    // создаём новый экземпляр запроса XHR
    /*xhr = new XMLHttpRequest();
    // открываем соединение
    xhr.open("GET", url, true, 'download', 'download');
    // устанавливаем заголовок — выбираем тип контента, который отправится на сервер, 
    // в нашем случае мы явно пишем, что это JSON
    xhr.setRequestHeader("Content-Type", "application/json");
    // когда придёт ответ на наше обращение к серверу
    xhr.onreadystatechange = proc;
    //xhr.send(JSON.strigify({ "name": "Ivan" }));
    xhr.send(null);
    console.log(xhr.status);*/

    //console.log('json:', data);
    xhr = $.ajax({
        url: url,
        dataType: 'json',
        data: data,
        type: 'POST',
        username: 'download',
        password: 'download',
        success: procSuccess,
        error: procError
    });
    window.xhr = xhr;
    window.xhr.url = url;
}

function procSuccess(jqXHR) {
    //console.log('procSuccess:', jqXHR);
    //console.log('procSuccess:', jqXHR.statusText);
    //console.log('procSuccess:', jqXHR.status);
    //showModalWindow();
    stopLoading();

    //console.log('procSuccess:', JSON.stringify(jqXHR));

    let modalContent = $('.modalContent');
    //let statusText = 'Status Text: ' + jqXHR.statusText;
    //let status = 'Status Code: ' + jqXHR.status;
    //$(modalContent).append('Успех: </br>');
    //$(modalContent).append(statusText + '</br>');
    //$(modalContent).append(status + '</br>');
    //$(modalContent).append(JSON.stringify(jqXHR));

    if (modalContent) $(modalContent).append(drawResultTable(jqXHR['Товары']));
}

function procError(jqXHR) {
    console.log('Error:', jqXHR);
    //console.log('Error, xhr:', xhr);
    stopLoading();
    $('#priceModal').append(jqXHR.statusText);

    let modalContent = $('.modalContent');
    if (modalContent) {
        let statusText = 'Status Text: ' + jqXHR.statusText;
        let status = 'Status Code: ' + jqXHR.status;
        let respText = 'Response Text: ' + jqXHR.responseText;
        $(modalContent).append('Ошибка: </br>');
        $(modalContent).append(statusText + '</br>');
        $(modalContent).append(status + '</br>');
        $(modalContent).append(respText);
    }
}

//draw modal
function showModalWindow() {
    let headerText = "Предварительный расчет цен:";

    let overlay = document.createElement('div');
    overlay.id = "mui-overlay";
    overlay.setAttribute('tabindex', '-1');
    $('body').append(overlay); //append(object or text)

    let modalContent = $('<div></div>').addClass("modalContent");
    modalContent.appendTo(overlay); //text

    //close btn
    let bClose = document.createElement('button');
    bClose.innerText = 'x';
    bClose.className = 'mui-btn mui-btn--fab mui-btn--small modalCloseBtn';
    //$(modalContent).append('<button class="mui-btn mui-btn--fab mui-btn--small modalCloseBtn">x</button>');
    $(modalContent).append(bClose);

    let header = $(`<h4>${headerText}</h4>`).addClass("modalHeader");
    header.appendTo(modalContent);

    $('<hr>').appendTo(modalContent);

    //event close
    overlay.onclick = (e) => {
        if (e.target.id === "mui-overlay")
            closeModal(e.target);
    }
    bClose.onclick = (e) => {
        closeModal(e.target.parentNode.parentNode);
    }
}

function drawResultTable(resObj) {
    //console.log('resObj: ', resObj);
    if (resObj) {
        let html = '<table id="priceTable" class="mui-table mui-table--bordered">';
        html += `<thead><tr>
            <th class="styleTableHead des">Номенклатура</th>
            <th class="styleTableHead consist" width="30px">Состояние</th>
            <th class="styleTableHead price" min-width="30px">Цена</th>
            <th class="styleTableHead count" width="30px">Кол-во</th>
            <th class="styleTableHead sum">Сумма</th>
        </tr><thead><tbody>`;

        let isHead = false;
        let sum = 0;

        //sum
        for (let index = 0; index < resObj.length; index++) {
            //category 0
            const el = resObj[index];
            if (el['Сумма']) sum += +el['Сумма'].toString().split(/\s+/g).join('');

            //elem children
            if (el.children) {
                for (let i = 0; i < el.children.length; i++) {
                    const child = el.children[i];
                    sum += +child['Сумма'].toString().split(/\s+/g).join('');
                }
            }
        }
        //итого
        let htmlSum = `<tr>
            <td class="des">Итого:</td>
            <td class="consist" width="20px"></td>
            <td class="price"></td>
            <td class="count" width="15px"></td>
            <td class="sum">${sum}</td>
        </tr>`;
        html += htmlSum;

        //view
        for (let index = 0; index < resObj.length; index++) {
            //category 0
            isHead = true;
            const el = resObj[index];

            if (el['Номенклатура']) {
                html += `<tr ${el['Состояние'] === "0" ? 'class="redColor"' : ""}>
                    <td class="des mainT">${el['Номенклатура']}</td>
                    <td class="consist" width="20px">${el['Состояние']}</td>
                    <td class="price">${el['Цена']}</td>
                    <td class="count" width="15px">${el['Количество']}</td>
                    <td class="sum">${el['Сумма']}</td>
                </tr>`;
            }

            //elem children
            if (el.children) {
                for (let i = 0; i < el.children.length; i++) {
                    const child = el.children[i];
                    html += `<tr ${child['Состояние'] === "0" ? 'class="redColor"' : ""}>
                        <td class="des">${child['Номенклатура']}</td>
                        <td class="consist" width="20px">${child['Состояние']}</td>
                        <td class="price">${child['Цена']}</td>
                        <td class="count" width="15px">${child['Количество']}</td>
                        <td class="sum">${child['Сумма']}</td>
                    </tr>`;
                    isHead = false;
                }
            }
        }
        html += htmlSum;

        return html + '</tbody></table>';
    }
    return "";
}

function closeModal(modal) {
    $(modal).remove();
}

//Loading anim
function startLoading() {
    $('.modalContent').append('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
}

function stopLoading() {
    $('.lds-ellipsis').remove();
}