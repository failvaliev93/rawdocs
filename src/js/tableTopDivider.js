/*
    Модуль делитель столешки. Длинномеры.
*/
/*
    window.d = myDlin
*/
import { stringToMap, upInt, compareNumeric, compareNumbers, compareNumericRev } from '@js/func.js'
import { topLOut } from '@js/rigaFunc.js'
import { specification } from '@/index.js'
import { RIGHE, jsonPrev } from '..';
//import * as $ from 'jquery' //костыль Для ивента

const keyVarGroupStone = ['TOP_MANUFACTURER', 'MERGE']; // теги каменных

//представление
export function tableTopDivider(RIGHE) {
    let $tableDiv = document.createElement('div');
    $tableDiv.className = 'dlinContainer';
    document.querySelector('body').appendChild($tableDiv);

    let dlinContent = document.createElement('div');
    dlinContent.className = "dlinContent mui-panel";
    dlinContent.innerHTML = `<h3 class="mui--text-center">Длинномеры:</h3>`

    $tableDiv.appendChild(dlinContent);

    //контент длинномеров
    var dlinCount = 1;
    //Столешки
    let keyVarGroup = ['TOP_MANUFACTURER', 'STOLESH_MAT', 'TOLSHINA', 'KROMKA', 'RADIUS',
        'TWO_SIDE_COLOR', 'TOP_FIND_P', 'KROMKA_COLOR'];
    dlinCount = dlinGroupDraw(RIGHE, dlinContent, keyVarGroup, 'TOP', "TOP_L", dlinCount);

    //стеновая панель 
    keyVarGroup = ['PANEL_PRODUCT', 'PANEL_COLOR', 'STP_L', 'KROMKA_COLOR', 'DONT_MERGE'];
    dlinCount = dlinGroupDraw(RIGHE, dlinContent, keyVarGroup, 'STP', "STP_L", dlinCount);

    //кнопка
    let button = document.createElement('button');
    button.id = "bAddChanges";
    button.className = "bAddChanges mui-btn mui-btn--raised mui-btn--primary";
    button.textContent = "Внести изменения";
    //button.disabled = true;
    dlinContent.appendChild(button);


    dlinContent.innerHTML += `<div class="mui--clearfix"></div>`

    //console.log(myDlin);

    //скрыть
    let close = document.createElement('div')
    close.className = "close"
    close.title = "Свернуть"
    close.innerHTML = '&#150;'
    let parW = $tableDiv.style.width
    let parH = $tableDiv.style.height
    close.addEventListener('click', (e) => {
        $tableDiv.style.width = "50px"
        $tableDiv.style.height = "50px"
        close.style.display = dlinContent.style.display = "none"
        open.style.display = "block"
    })
    $tableDiv.appendChild(close)

    //открыть
    let open = document.createElement('div')
    open.className = "open"
    open.title = "Развернуть"
    open.innerHTML = '&#43;'
    open.addEventListener('click', (e) => {
        $tableDiv.style.width = parW
        $tableDiv.style.height = parH
        open.style.display = "none"
        close.style.display = dlinContent.style.display = "block"
    })
    $tableDiv.appendChild(open)

    //наличие столешниц в проекте
    if (myDlin['1']) {
        //events:
        //кнопка готово 
        document.getElementById('bAddChanges').addEventListener("click", (event) => {
            dlinBAddChanges(RIGHE, event);
        });

        //режим
        let selDlins = document.querySelectorAll('.selDlin');
        for (let index = 0; index < selDlins.length; index++) {
            selDlins[index].addEventListener("change", function (event) {
                let value = selDlins[index].options[selDlins[index].options.selectedIndex].value;
                let id = +selDlins[index].id.replace(selDlins[index].className, "");
                let dlin = myDlin[id];
                let dlinGroup = document.querySelector('.dlinGroup' + id); //??? (event)
                dlinSelDraw(RIGHE, value, dlin, dlinGroup);
                dlin.activeDlin = value;

                let button = document.getElementById('bAddChanges');
                button.disabled = false;
                if (!button.disabled) button.textContent = "Внести изменения";

                let $tabOptOutL = document.getElementById("tabOptOutL" + id);
                controlCalc(RIGHE, id, $tabOptOutL);
            });
        }
    } else {
        document.querySelector('.dlinContainer').outerHTML = ""
    }

    window.d = myDlin;
    specification(jsonPrev);

    //подсветка
    let pList = $tableDiv.getElementsByClassName('tabFindIn');
    for (let i = 0; i < pList.length; ++i) {
        for (let j = 0; j < pList[i].children.length; ++j) {
            pList[i].children[j].onmouseenter = backlightElem;
            pList[i].children[j].onmouseleave = backlightElemLeave;
        }
    }
    let pListOpt = $tableDiv.getElementsByClassName('tabOptIn');
    for (let i = 0; i < pListOpt.length; ++i) {
        for (let j = 0; j < pListOpt[i].children.length; ++j) {
            pListOpt[i].children[j].onmouseenter = backlightElem;
            pListOpt[i].children[j].onmouseleave = backlightElemLeave;
        }
    }
    //pList.forEach(item => {
    //item.onmouseenter = backlightElem;
    //});
}


function backlightElem(e) {
    //console.log(e.target);
    let el = e.target;
    let tbl = document.querySelector('#mytable');
    let matches = tbl.querySelectorAll('tr[data-uniid]'); //table
    for (let i = 0; i < matches.length; ++i) {
        let count = matches[i].querySelector('.count').innerText;
        if (el.dataset.uniid && el.dataset.uniid === matches[i].dataset.uniid
            && el.dataset.count === count) {
            matches[i].classList.add("backlight");
            //matches[i].style.boxShadow = "0px 0px 10px red";
            //console.log(matches[i]);
        }
    }
    //el.classList.add("backlight");
}

function backlightElemLeave(e) {
    let tbl = document.querySelector('#mytable');
    let matches = tbl.querySelectorAll('tr[data-uniid]');
    for (let i = 0; i < matches.length; ++i) {
        //matches[i].style.boxShadow = "";
        matches[i].classList.remove("backlight");
    }
    //el.classList.remove("backlight");
}

function test(event) {
    event.preventDefault();
    console.log(event);
}

//класс
function ModuleDlin() {
    this.oldDlin = [];    //пришло изначально
    this.autoDlin = [];   //режим авто
    this.handleDlin = []; //режим ручной
    this.id = 0;          //идентификатор
    this.cod = "";       //столешка, ст. панель... 
    this.arrL = [];       //список пришедших кусков
    this.activeDlin = EDlinSelValue.auto;//запускаю с просчетом
    this.disabledButton = false;
    this.elem = [];   //внутренности длин-ра
    this.getCurrentArr = function () {
        if (this.activeDlin == EDlinSelValue.auto) return this.autoDlin;
        else if (this.activeDlin == EDlinSelValue.noChange) return this.oldDlin;
        else if (this.activeDlin == EDlinSelValue.handle) return this.handleDlin;
    }
}

// ModuleDlin.getCurrentArr = function () {
//     if (ModuleDlin.activeDlin == EDlinSelValue.auto) return ModuleDlin.autoDlin;
//     else if (ModuleDlin.activeDlin == EDlinSelValue.noChange) return ModuleDlin.oldDlin;
//     else if (ModuleDlin.activeDlin == EDlinSelValue.handle) return ModuleDlin.handleDlin;
// }

//Enum: 3 значения у Select
let EDlinSelValue = {
    auto: "auto",
    noChange: "noChange",
    handle: "handle"
}

let myDlin = Object.create(null); //содержит все объекты длин-в

//группирую полученные столешки
function dlinToGroup(RIGHE, keyVarGroup, cod) {
    //const keyVarGroup = ['TOP_MANUFACTURER', 'STOLESH_MAT', 'TOLSHINA', 'KROMKA', 'RADIUS', 'TWO_SIDE_COLOR'];

    var top = _.filter(RIGHE, { 'COD': cod });
    //Перебираем массив столешниц
    top = top.map(function (item) {
        //Преобразуем сторку вариантов в массив
        item.VarArr = stringToMap(item.VAR);
        return item;
    });
    //Группируем столешницы по вариантам
    var topGroup = _.groupBy(top, function (item) {
        //подбираем варианты для объединения столшниц
        //Фильтруем только нужные варианты
        var GroupKey = item.VarArr.filter(function (iVar) {
            return _.indexOf(keyVarGroup, iVar[0]) >= 0
        });
        //Преобразуем в строку <вариант>=<Значение>
        GroupKey = GroupKey.map(function (ikey) {
            return _.join(ikey, '=');
        });
        //объединяем все варинты в троку с разделителем ";"
        return _.join(GroupKey, ';');
    });
    return topGroup
}

//интерфейс группы
//cod - 'TOP'
//dlinContent - node
//keyVarGroup - const vars
//strOfL - 'TOP_L'
function dlinGroupDraw(RIGHE, dlinContent, keyVarGroup, cod, strOfL, dlinCount) {
    let container = ""
    let topGroup = dlinToGroup(RIGHE, keyVarGroup, cod);
    topGroup = reGroup(topGroup);
    let sumL = 0;
    //let dlinCount = 1;

    //для каменных
    function reGroup(topGroup) {
        if (cod === "TOP") {
            let isHaveStone = false;
            for (const key in topGroup) {
                if (Object.hasOwnProperty.call(topGroup, key)) {
                    const TOP_MANUFACTURER = RIGHE.getTag(topGroup[key][0], "TOP_MANUFACTURER");
                    if (TOP_MANUFACTURER === "SUMSUNG" || TOP_MANUFACTURER === "HIMACS") {
                        isHaveStone = true;
                        delete topGroup[key];
                        break;
                    }
                }
            }
            //весь камень объединяю в одну группу по произв-ю           
            if (isHaveStone) {
                var top = _.filter(RIGHE, { 'COD': cod });

                //Группируем столешницы по вариантам
                var topGroup1 = _.groupBy(top, function (item) {
                    //подбираем варианты для объединения столшниц
                    //Фильтруем только нужные варианты
                    const TOP_MANUFACTURER = RIGHE.getTag(item, "TOP_MANUFACTURER");
                    //if (TOP_MANUFACTURER === "SUMSUNG" || TOP_MANUFACTURER === "HIMACS") 

                    var GroupKey = item.VARS.filter(function (iVar) {
                        if (TOP_MANUFACTURER === "SUMSUNG" || TOP_MANUFACTURER === "HIMACS") {
                            return _.indexOf(keyVarGroupStone, iVar[0]) >= 0
                        } else {
                            return _.indexOf(keyVarGroup, iVar[0]) >= 0
                        }
                    });
                    //Преобразуем в строку <вариант>=<Значение>
                    GroupKey = GroupKey.map(function (ikey) {
                        return _.join(ikey, '=');
                    });
                    //объединяем все варинты в троку с разделителем ";"
                    return _.join(GroupKey, ';');
                });
                return topGroup1;
            }
        }
        return topGroup;
    }

    for (const key in topGroup) {
        if (topGroup.hasOwnProperty(key)) {
            sumL = 0;
            const el = topGroup[key];

            //запоминаю группу
            let dlin = new ModuleDlin();
            dlin.id = dlinCount;
            dlin.cod = el[0].COD;

            //длины столешки
            let arrL = RIGHE.getTag(el[0], strOfL).split('*').map(item => Number(item));
            //работаю с пришедшими цифрами //отменено:перевожу L к 1С
            // if (dlin.cod == "TOP") {
            //     for (let j = 0; j < arrL.length; j++) {
            //         arrL[j] = topLOut(arrL[j], el[0], RIGHE);
            //     }
            // }
            //console.log(arrL);
            arrL.sort(compareNumeric);
            dlin.arrL = arrL;

            //старые столешки oldDlin
            let find = "";
            let n = 1;
            for (let i = 0; i < el.length; i++) {
                let classCss = "even"; //цвет фона четной
                if (n % 2 == 0) classCss = "odd"; //не четная
                /*find += '<p class="' + classCss + '" data-uniid="' + el[i].MY_UNI_ID + '" \
                >' + '1' + ' длиной ' + el[i].MY_REAL_L + '\
                 ('+ el[i].L + ')</p>';*/
                find += `<p class="${classCss}" data-uniid="${el[i].MY_UNI_ID}" \
                data-count="${el[i].PZ}" data-len="${el[i].L}"
                >1 длиной ${el[i].MY_REAL_L} (${el[i].L})</p>`;
                n++;
                sumL += el[i].L;
                dlin.oldDlin.push(RIGHE.createBased(el[i]));
                dlin.handleDlin.push(RIGHE.createBased(el[i]));
            }

            //копирую внутренности длин-ра по riga.IDBOX
            for (let i = 0; i < dlin.oldDlin.length; i++) {
                for (const key in RIGHE) {
                    if (RIGHE.hasOwnProperty(key)) {
                        if (dlin.oldDlin[i].IDBOX == RIGHE[key].IDBOX &&
                            RIGHE[key].COD != cod)
                            dlin.elem.push(RIGHE.createBased(RIGHE[key]));
                    }
                }
            }
            //меняю elem.IDBOX на IDBOX группы
            // т.к. они не удаляются
            for (let i = 0; i < dlin.elem.length; i++) {
                let groupIdbox = dlin.oldDlin[0].IDBOX;
                dlin.elem[i].IDBOX = groupIdbox;
            }

            //AUTO
            dlinCalcSelAuto(RIGHE, dlin);

            dlinSelSetDefault(dlin);

            let dlinGroup = document.createElement('div');
            dlinGroup.className = "dlinGroup" + dlin.id;

            dlinGroup.innerHTML += `
        <div class="mui-divider"></div>
        <div class="dlinHead">${topGroup[key][0].MY_DES.split('. Д')[0]}
        <select id="selDlin${dlin.id}" class="selDlin" size="1">
            <option value="${EDlinSelValue.auto}" ${dlin.activeDlin == EDlinSelValue.auto ? "selected" : ""}>Автоматически</option>
            <option value="${EDlinSelValue.noChange}" ${dlin.activeDlin == EDlinSelValue.noChange ? "selected" : ""}>Без изменений</option>
            <option value="${EDlinSelValue.handle}" ${dlin.activeDlin == EDlinSelValue.handle ? "selected" : ""}>Ручной</option>
        </select>
        </div>      
        <div class="tab">
            <div class="tabFind">
                Найдено:
                <div class="tabFindIn" id="${dlin.id}_tabFindIn">${find}</div>
            </div>
            <div class="tabOpt">
                Оптимизировано:
                <div class="tabOptIn">
                    ${/*dlinSelDraw(RIGHE, dlin.activeDlin, dlin)*/""}
                </div>
            </div>
            <div class="mui--clearfix"></div>
        </div>
        <div class="total">
            <div class="tabFind tabFindOut" id="tabFindOut${dlin.id}">
                <p class="">Итого: <span id="tabFindOutL${dlin.id}">
                    ${sumL}
                </span></p>
            </div>
            <div class="tabOptOut" id="tabOptOut${dlin.id}">
                <p class="totalGood">Итого: 
                    <span id="tabOptOutL${dlin.id}" class="tabOptOutL">
                </p>
            </div>
            <div class="mui--clearfix"></div>
        </div>`;

            dlinContent.appendChild(dlinGroup);

            myDlin[dlinCount] = dlin;
            dlinCount++;

            //console.log(dlin.id, dlin);
            //предварительное заполнение в оптимизировано
            //console.log(document.getElementById('selDlin' + dlin.id));
            dlinSelDraw(RIGHE, dlin.activeDlin, dlin, dlinGroup); //dlin.activeDlin
        }
    }

    return dlinCount
}

//интерфейс выбора просчета - представление
//value - текущий режим, dlin.activeDlin
//dlin - ModuleDlin{}
//dlinGroup - контейнер div
function dlinSelDraw(RIGHE, value, dlin, dlinGroup) {
    let container = "";
    let sumL = 0;
    let thisArr = [];
    let $pcontainer = dlinGroup.querySelector('.tabOptIn');
    let $total = dlinGroup.querySelector('.tabOptOutL');
    $pcontainer.innerHTML = "";
    $total.innerHTML = "0";

    if (value === EDlinSelValue.auto) {
        thisArr = dlin.autoDlin
        let n = 1;
        for (let i = 0; i < thisArr.length; i++) {
            let classCss = "even"; //цвет фона четной
            if (n % 2 == 0) classCss = "odd"; //не четная
            container += `<p class=${classCss} data-uniid="${thisArr[i].MY_UNI_ID}"\
            data-count="${thisArr[i].PZ}" data-len="${thisArr[i].L}"\
            >${thisArr[i].PZ} длиной ${thisArr[i].L} </p>`;
            n++;
            sumL += thisArr[i].L * thisArr[i].PZ;

        }
        $pcontainer.innerHTML = container;
        $total.innerHTML = sumL;

    } else if (value === EDlinSelValue.noChange) {
        thisArr = dlin.oldDlin;

    } else if (value === EDlinSelValue.handle) {
        thisArr = dlin.handleDlin;
        let n = 1;

        for (let i = 0; i < thisArr.length; i++) {
            let classCss = "even"; //цвет фона четной
            if (n % 2 == 0) classCss = "odd"; //не четная

            let $par = document.createElement("p");
            $par.className = classCss;
            $par.dataset.row = i;
            $par.dataset.id = dlin.id;
            $par.dataset.uniid = thisArr[i].MY_UNI_ID;
            $par.dataset.count = thisArr[i].PZ;
            $par.dataset.len = thisArr[i].L;

            //список кол-ва //selected
            let $selCount = document.createElement('select')
            $selCount.className = "selCount";
            for (let j = 1; j < 3; j++) {
                let isSelected = false;
                if (j == thisArr[i].QTA) { isSelected = true };
                let opt = new Option(j, j, isSelected, isSelected);
                $selCount.add(opt);
            }
            $par.appendChild($selCount);

            $par.innerHTML += " длиной ";

            //список размеров
            let $selL = document.createElement('select');
            $selL.className = "selL";
            for (let j = 0; j < dlin.arrL.length; j++) {
                let isSelected = false;
                //перевожу L к 1С
                //dlin.arrL[j] = topLOut(dlin.arrL[j], thisArr[i], RIGHE);
                if (dlin.arrL[j] == thisArr[i].L) { isSelected = true };
                let opt = new Option(dlin.arrL[j], dlin.arrL[j], isSelected, isSelected);
                $selL.add(opt);
            }
            $par.appendChild($selL);

            sumL += thisArr[i].L * thisArr[i].QTA;
            $total.innerHTML = sumL;

            controlCalc(RIGHE, dlin.id, $total);

            //Y - удалить
            let $del = document.createElement('span');
            $del.className = "delete";
            $del.innerHTML = " &#150 ";
            $del.title = "Удалить";
            if (n > 0) { //первый нельзя удалить //изменено можно 07,12,2020
                $par.appendChild($del);
            }

            //events:
            //длины
            $selL.addEventListener("change", function (event) {
                let idTop = event.target.parentElement.dataset.row;
                let id = +event.target.parentElement.dataset.id;
                let riga = myDlin[id].handleDlin[idTop];
                let oldL = riga.L;

                myDlin[id].handleDlin[idTop].L = +event.target.value;
                riga.MY_DES = riga.MY_DES.replace(oldL, riga.L);

                let sumL = 0;
                for (let i = 0; i < myDlin[id].handleDlin.length; i++) {
                    sumL += myDlin[id].handleDlin[i].L * myDlin[id].handleDlin[i].QTA;
                }
                let $tabOptOutL = document.getElementById("tabOptOutL" + id);
                $tabOptOutL.innerHTML = sumL;
                //console.log("idTop:", idTop, ",id:", id, "value:", myDlin[id].handleDlin[idTop].L);

                controlCalc(RIGHE, id, $tabOptOutL);
            });

            $pcontainer.appendChild($par);

            //кол-ва 
            $par.querySelector(".selCount").addEventListener("change", (event) => {
                let idTop = event.target.parentElement.dataset.row;
                let id = +event.target.parentElement.dataset.id;
                myDlin[id].handleDlin[idTop].QTA = +event.target.value;
                myDlin[id].handleDlin[idTop].PZ = +event.target.value;

                let sumL = 0;
                for (let i = 0; i < myDlin[id].handleDlin.length; i++) {
                    sumL += myDlin[id].handleDlin[i].L * myDlin[id].handleDlin[i].QTA;
                }
                let $tabOptOutL = document.getElementById("tabOptOutL" + id);
                $tabOptOutL.innerHTML = sumL;
                //console.log("idTop:", idTop, ",id:", id, "value:", myDlin[id].handleDlin[idTop].QTA);

                controlCalc(RIGHE, id, $tabOptOutL);
            });

            //удалить
            $del.addEventListener("click", (event) => {
                let idTop = event.target.parentElement.dataset.row;
                let id = +event.target.parentElement.dataset.id;
                myDlin[id].handleDlin.splice(idTop, 1);
                dlinSelDraw(RIGHE, value, dlin, dlinGroup);
            });

            n++;
        }

        //тут "+"
        /*let classCss = "even"; //цвет фона четной
        if (n % 2 == 0) classCss = "odd"; //не четная

        let $par = document.createElement("p");
        $par.classList = "odd" + " plus mui--text-right";
        $par.dataset.row = n - 1;
        $par.dataset.id = dlin.id;
        $par.innerHTML = '&#43';
        $par.title = "Добавить";

        //event добавить
        $par.addEventListener("click", (event) => {
            //создаю новую или копирую riga из пришедших 
            //if (dlin.oldDlin.length < dlin.handleDlin.length) 
            let riga = RIGHE.createBased(dlin.handleDlin[0]);
            riga.MY_UNI_ID = RIGHE.setUniId();

            dlin.handleDlin.push(riga);

            dlinSelDraw(RIGHE, value, dlin, dlinGroup);
        });

        $pcontainer.appendChild($par);*/
    }
}

//автоустановка выбора просчета
function dlinSelSetDefault(dlin) {
    //Для этих ниже условий уже вношу изменения и меняю режим
    // при условии что в группе все похожие
    const el = dlin.oldDlin[0];
    const TOP_MANUFACTURER = RIGHE.getTag(el, "TOP_MANUFACTURER").toUpperCase();
    const TOP_FORM_COD = RIGHE.getTag(el, "TOP_FORM_COD");
    const KROMKA = RIGHE.getTag(el, "KROMKA");

    if (el.COD === "TOP") {
        if (TOP_MANUFACTURER === "SUMSUNG" || TOP_MANUFACTURER === "HIMACS") {
            //камень
            dlin.activeDlin = EDlinSelValue.auto;

        } else if (TOP_MANUFACTURER === "SOYUZ" || TOP_MANUFACTURER === "KEDR"
            || TOP_MANUFACTURER === "SLOTEX" || TOP_MANUFACTURER === "ANTARES") {

            if (TOP_FORM_COD === "01" && KROMKA === "ABS" &&
                (TOP_MANUFACTURER === "SLOTEX" || TOP_MANUFACTURER === "ANTARES"
                    || TOP_MANUFACTURER === "SOYUZ")) {
                //собственный ABS
                dlin.activeDlin = EDlinSelValue.noChange;
            } else if (TOP_FORM_COD === "01") {
                //прямые
                dlin.activeDlin = EDlinSelValue.auto;
            } else {
                //остальные формы
                dlin.activeDlin = EDlinSelValue.noChange;
            }
        }
    } else if (el.COD === 'STP') {
        const DONT_MERGE = RIGHE.getTag(el, 'DONT_MERGE');
        if (DONT_MERGE) dlin.activeDlin = EDlinSelValue.noChange;
    }
    changeTable(dlin);
}

//авторасчет
function dlinCalcSelAuto(RIGHE, dlin) {
    let sumLReal = 0, sumLReal2 = 0; //сумма кусков
    let arrLCount = []; //кол-во кусков
    let arrL = dlin.arrL;
    let newArrDlin = [];

    //сумма кусков
    for (let i = 0; i < dlin.oldDlin.length; i++) {
        const el = dlin.oldDlin[i];
        sumLReal += el.MY_REAL_L;
    }
    sumLReal2 = sumLReal;

    //иниц-я кол-ва кусков
    for (let i = 0; i < arrL.length; i++) {
        arrLCount.push(0);
    }

    //расчет кол-ва кусков
    //***самый первый вариант***
    /*for (let i = arrL.length - 1; i >= 0; i--) {
        if (arrL[i] <= sumLReal) { //кусок влезает
            let countI = Math.floor(sumLReal / arrL[i]);
            sumLReal -= arrL[i] * countI;
            //arrLCount.push(countI);
            arrLCount[i] = countI
        } else {
            //arrLCount.push(0);
        }
    }
    if (sumLReal > 0) { //остался кусочек 
        arrLCount[0] += 1;
    }*/

    //*** Иринкин ***//
    //console.log("arrL", arrL);//- листы
    //arrLCount - список кол. листов
    let findArr = []; //список кусков
    for (let i = 0; i < dlin.oldDlin.length; i++) {
        const el = dlin.oldDlin[i];
        findArr.push(el.MY_REAL_L);
    }
    findArr.sort(compareNumericRev);
    toComplect(arrL, findArr, arrLCount);

    /*
        arrL - листы
        findArr - список кусков
        вывод arrLCount - список кол. листов
     */
    function toComplect(arrL, findArr, arrLCount) {
        let ostatok = [[], [], []]; //остатки

        //1) Начинаем проверять каждый кусок, начиная с большего (2700), вычитая из возможных листов
        let x = findArr[0];
        findArr.splice(0, 1); //сразу избавляемся от куска
        for (let i = 0; i < arrL.length; i++) {
            const ost = arrL[i] - x;
            if (ost >= 0) {
                ostatok[0].push(ost); //длина остатка
                ostatok[1].push(arrL[i]); //полная длина листа
                ostatok[2].push(i); //номер листа
            }
        }

        //console.log("1) ostatok", ostatok);
        //console.log("1) findArr", findArr);

        //2) Проверяем, входит какой-либо из оставшихся кусков (2200, 1400, 300) в каждый из остатков от листов? 
        let indicator = true;
        top: //label
        for (let i = 0; i < findArr.length; i++) { //листы
            const piece = findArr[i];
            for (let j = 0; j < ostatok[0].length; j++) {
                const ost = ostatok[0][j] - piece;
                if (ost > 0) {
                    findArr.splice(i, 1); //избавляемся от куска
                    arrLCount[ostatok[2][j]] += 1; //записываю кусок
                    //debugger;
                    ostatok[0][j] = ost;
                    //избавляемся от других кусков
                    ostatok[0].splice(0, j);
                    ostatok[1].splice(0, j);
                    ostatok[2].splice(0, j);
                    ostatok[0].splice(j + 1, ostatok[0].length);
                    ostatok[1].splice(j + 1, ostatok[1].length);
                    ostatok[2].splice(j + 1, ostatok[2].length);
                    indicator = false;

                    break top;
                }
            }
        }
        //если не смогли запихнуть в остатки ничего
        if (indicator) {
            for (let i = 0; i < arrL.length; i++) {
                const ost = arrL[i] - x;
                if (ost >= 0) {
                    arrLCount[i] += 1;
                    ostatok.splice(0, ostatok.length);
                    ostatok.push([ost]);
                    ostatok.push([arrL[i]]);
                    ostatok.push([i]);
                    break;
                }
            }
        }

        //console.log("2) ostatok", ostatok);
        //console.log("2) findArr", findArr);
        //console.log("2) arrLCount", arrLCount);

        //3.1) Оставшиеся куски в остатки
        if (findArr.length > 0) {
            for (let i = 0; i < findArr.length; i++) {
                const piece = findArr[i];
                const ost = ostatok[0][0] - piece;
                //кусок влез
                if (ost >= 0) {
                    findArr.splice(i, 1); //избавляемся от куска
                    break;
                }
            }
            //console.log("3) ostatok", ostatok);
            //console.log("3) findArr", findArr);
        }

        //3.2) Оставшиеся куски сначало
        if (findArr.length > 0) {
            for (let i = 0; i < findArr.length; i++) {
                toComplect(arrL, findArr, arrLCount);
            }
        }
    }

    //console.log(arrLCount);
    //если 2 по 2000 умещаются в 4000
    for (let i = 0; i < arrL.length; i++) {
        const min = arrL[i];
        if (arrLCount[i] > 1) {
            for (let j = 0; j < arrL.length; j++) {
                const max = arrL[j];
                if (max > 0 && min * 2 === max) {
                    //console.log(min, max);
                    ++arrLCount[j];//++max;
                    --arrLCount[i];//--min;
                    --arrLCount[i];
                }
            }
        }
    }

    //перевожу L к 1С
    //const el = dlin.oldDlin[0];
    if (dlin.cod == "TOP") {
        for (let j = 0; j < dlin.arrL.length; j++) {
            dlin.arrL[j] = topLOut(dlin.arrL[j], dlin.oldDlin[0], RIGHE);
        }
    }
    //console.log(arrLCount);

    //массив новых кусков
    for (let i = 0; i < arrLCount.length; i++) {
        if (arrLCount[i] > 0) {
            let riga = {}
            let oldDlinId;
            if (dlin.oldDlin[i]) {
                riga = RIGHE.createBased(dlin.oldDlin[i]);
                oldDlinId = i;
            } else {
                oldDlinId = 0;
                riga = RIGHE.createBased(dlin.oldDlin[0]);
                riga.MY_UNI_ID = RIGHE.setUniId();
            }

            riga.L = arrL[i]; //длина
            riga.MY_REAL_L = sumLReal2;
            riga.PZ = arrLCount[i]; //количество               

            //меняю в описание длину
            riga.MY_DES = riga.MY_DES.replace(dlin.oldDlin[oldDlinId].L, riga.L);
            newArrDlin.push(riga);
        }
    }
    dlin.autoDlin = newArrDlin;
}

//Проверка в ручном режиме и отключение кнопки Внести //Отключено
function controlCalc(RIGHE, id, $tabOptOutL) {
    //28.12.2020 отключил это условие полностью
    /*
    let $tabFindOutL = document.getElementById("tabFindOutL" + id);
    let findL = +$tabFindOutL.innerText;
    let optL = +$tabOptOutL.innerText;
    let $button = document.getElementById('bAddChanges');

    if (findL < optL - 1000) {
        myDlin[id].disabledButton = true;
        $tabOptOutL.classList.add("totalBad");
    } else {
        myDlin[id].disabledButton = false;
        $tabOptOutL.classList.remove("totalBad");
    }

    //достаточно 1 ошибки чтоб выключить кнопку
    $button.disabled = false;
    for (const key in myDlin) {
        if (myDlin[key].disabledButton == true) {
            $button.disabled = true;
        }
    }
    */
}

//внести изменения с перерисовкой
function changeAndDrawTable(dlin) {
    //debugger
    changeTable(dlin);
    specification(jsonPrev);
}

//внести изменения
function changeTable(dlin) {
    const arr = dlin.getCurrentArr();

    if (arr) { //&& arr.length > 0
        //вычищаю
        for (let i = 0; i < dlin.oldDlin.length; i++) {
            for (const key in RIGHE) {
                if (RIGHE.hasOwnProperty.call(RIGHE, key)) {
                    const el = RIGHE[key];
                    //элемент
                    if (el.MY_UNI_ID === dlin.oldDlin[i].MY_UNI_ID ||
                        (dlin.autoDlin[i] && el.MY_UNI_ID === dlin.autoDlin[i].MY_UNI_ID) ||
                        (dlin.handleDlin[i] && el.MY_UNI_ID === dlin.handleDlin[i].MY_UNI_ID)) {
                        //debugger; 
                        delete RIGHE[key];
                    }
                    //с таким же IDBOX
                    //if (el.IDBOX === dlin.oldDlin[i].IDBOX)
                    //delete RIGHE[key];
                }
            }
        }

        if (arr.length > 0) {
            //добавляю
            for (let i = 0; i < arr.length; i++) {
                //debugger;
                RIGHE.addNewToEnd(arr[i]);

                //с таким же IDBOX
                //for (let j = 0; j < dlin.elem.length; j++) {
                //if (dlin.elem[j].IDBOX === arr[i].IDBOX)
                //RIGHE.addNewToEnd(dlin.elem[j]);
                //}
            }
        }
    }
}

//кнопка внести изменения
function dlinBAddChanges(RIGHE, event) {
    event.preventDefault();
    //console.log("Ты нажал на МЕНЯ!!1");

    let button = event.target;
    button.disabled = true;
    //button.textContent = "Готово"

    for (const key in myDlin) {
        const el = myDlin[key];
        //let thisArr = [];
        //thisArr = el.getCurrentArr();
        //console.log(el);
        changeTable(el);
        //if (el.activeDlin == EDlinSelValue.auto) thisArr = el.autoDlin;
        //else if (el.activeDlin == EDlinSelValue.noChange) thisArr = el.oldDlin;
        //else if (el.activeDlin == EDlinSelValue.handle) thisArr = el.handleDlin;

        //удаляем по IDBOX
        /*for (let i = 0; i < el.oldDlin.length; i++) {
            for (const key in RIGHE) {
                if (RIGHE.hasOwnProperty(key)) {
                    if (RIGHE[key].IDBOX == el.oldDlin[i].IDBOX) {
                        delete RIGHE[key];
                    }
                }
            }
        }

        //добавляем
        for (let i = 0; i < thisArr.length; i++) {
            RIGHE.addNewToEnd(thisArr[i])
        }
        for (let i = 0; i < el.elem.length; i++) {
            RIGHE.addNewToEnd(el.elem[i])
        }*/
    }

    specification(jsonPrev);

    //console.log(myDlin);
    //console.log(RIGHE);
}